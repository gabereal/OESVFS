#pragma once
#include "gmock/gmock.h"
#include "VFSPathLoader.h"

class MockVFSPathLoader : public oesvfs::VFSPathLoader
{
public:
	MockVFSPathLoader(oesvfs::VFSRoot& p_root) : VFSPathLoader(p_root) {};

	MOCK_METHOD1(CanLoad, bool(oesvfs::VFSDirectory&));
	MOCK_METHOD1(CanLoad, bool(oesvfs::VFSFile&));
	MOCK_METHOD1(CanReadPath, bool(std::string));

	MOCK_METHOD3(CreateFile, std::shared_ptr<oesvfs::VFSFile>(oesvfs::VFSDirectory&, std::string, std::istream&));
	MOCK_METHOD3(Rename, bool(oesvfs::VFSDirectory&, oesvfs::VFSEntry&, std::string));

	MOCK_METHOD1(IsDirectory, bool(oesvfs::VFSEntry&));
	MOCK_METHOD1(IsFile, bool(oesvfs::VFSEntry&));
	MOCK_METHOD1(CanRead, bool(oesvfs::VFSEntry&));
	MOCK_METHOD1(CanWrite, bool(oesvfs::VFSEntry&));

	MOCK_METHOD2(GetInputStream, std::shared_ptr<std::istream>(oesvfs::VFSFile&, std::ios_base::openmode));
	MOCK_METHOD2(GetOutputStream, std::shared_ptr<std::ostream>(oesvfs::VFSFile&, std::ios_base::openmode));

	MOCK_METHOD2(CloseStream, void(oesvfs::VFSFile&, std::shared_ptr<std::istream>&));
	MOCK_METHOD2(CloseStream, void(oesvfs::VFSFile&, std::shared_ptr<std::ostream>&));

	MOCK_METHOD1(OpenFile_, std::shared_ptr<oesvfs::VFSDirectory>(oesvfs::VFSFile&));

	MOCK_METHOD2(CreateDirectory_, std::shared_ptr<oesvfs::VFSDirectory>(oesvfs::VFSDirectory&, std::string));
	MOCK_METHOD2(CreateFile_, std::shared_ptr<oesvfs::VFSFile>(oesvfs::VFSDirectory&, std::string));

	MOCK_METHOD1(Load_, bool(oesvfs::VFSFile&));
	MOCK_METHOD1(Load_, bool(oesvfs::VFSDirectory&));

	MOCK_METHOD2(CopyFile_, void(oesvfs::VFSFile&, std::shared_ptr<oesvfs::VFSFile>));
	MOCK_METHOD2(CopyFile_, void(oesvfs::VFSFile&, std::shared_ptr<oesvfs::VFSDirectory>));
	MOCK_METHOD2(CopyDirectory_, void(oesvfs::VFSDirectory&, std::shared_ptr<oesvfs::VFSDirectory>));

	MOCK_METHOD1(Size_, std::uint64_t(oesvfs::VFSDirectory&));
	MOCK_METHOD1(Size_, std::uint64_t(oesvfs::VFSFile&));

	MOCK_METHOD2(Remove_, bool(oesvfs::VFSDirectory&, oesvfs::VFSDirectory&));
	MOCK_METHOD2(Remove_, bool(oesvfs::VFSDirectory&, oesvfs::VFSFile&));
};

struct VFSEntryFactory
{
	static std::shared_ptr<oesvfs::VFSEntry> CreateEntry(std::string p_name, std::shared_ptr<oesvfs::VFSPathLoader> p_loader)
	{
		return std::shared_ptr<oesvfs::VFSEntry>(new VFSTestEntry(p_name, p_loader));
	}

	class VFSTestEntry : public oesvfs::VFSEntry
	{
	public:
		VFSTestEntry(std::string p_name, std::shared_ptr<oesvfs::VFSPathLoader> p_loader) : VFSEntry(p_name, p_loader)
		{};
	};
};