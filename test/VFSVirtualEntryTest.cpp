#include "VFSVirtualEntryTest.h"
#include "mock_VFSPathLoader.h"
#include "VFSRoot.h"

using namespace oesvfs;

TEST_F(VFSVirtualEntryTest, ByDefaultTheObjectContainsNoEntries)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.GetEntry(), nullptr);
}

TEST_F(VFSVirtualEntryTest, ByDefaultTheObjectHasNoName)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.GetName(), "");
}

TEST_F(VFSVirtualEntryTest, ByDefaultTheObjectHasNoPath)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.GetFullPath(), "");
}

TEST_F(VFSVirtualEntryTest, TestVirtualPath)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.GetFullVirtualPath(), "testPath");
}

TEST_F(VFSVirtualEntryTest, TestVirtualName)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.GetFullVirtualPath(), "testPath");
}

TEST_F(VFSVirtualEntryTest, TestSizeOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.FileSize(), 0);
}

TEST_F(VFSVirtualEntryTest, TestRemoveOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.Remove(), false);
}

TEST_F(VFSVirtualEntryTest, TestRenameOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.Rename(""), false);
	EXPECT_EQ(vEntry.Rename("ajhbdajhdgahsj"), false);
}

TEST_F(VFSVirtualEntryTest, TestExistsOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.Exists(), false);
}

TEST_F(VFSVirtualEntryTest, TestIsDirectoryOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.IsDirectory(), false);
}

TEST_F(VFSVirtualEntryTest, TestIsFileOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.IsFile(), false);
}

TEST_F(VFSVirtualEntryTest, TestCanReadOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.CanRead(), false);
}

TEST_F(VFSVirtualEntryTest, TestCanWriteOfNullEntry)
{
	VFSVirtualEntry vEntry(nullptr, "testPath", "testName");
	EXPECT_EQ(vEntry.CanWrite(), false);
}

TEST_F(VFSVirtualEntryTest, TestGetEntry)
{
	VFSRoot root;
	auto loader = std::shared_ptr<VFSPathLoader>(new MockVFSPathLoader(root));
	auto entry = VFSEntryFactory::CreateEntry("testEntry", loader);
	VFSVirtualEntry vEntry(entry, "testPath", "testName");
	EXPECT_EQ(vEntry.GetEntry(), entry);
}

TEST_F(VFSVirtualEntryTest, TestSetEntry)
{
	VFSRoot root;
	auto loader = std::shared_ptr<VFSPathLoader>(new MockVFSPathLoader(root));
	auto entry = VFSEntryFactory::CreateEntry("testEntry", loader);
	VFSVirtualEntry vEntry(VFSEntryFactory::CreateEntry("testEntry", loader), "testPath", "testName");
	vEntry.SetEntry(entry);
	EXPECT_EQ(vEntry.GetEntry(), entry);
}

TEST_F(VFSVirtualEntryTest, TestGetName)
{
	VFSRoot root;
	auto loader = std::shared_ptr<VFSPathLoader>(new MockVFSPathLoader(root));
	auto entry = VFSEntryFactory::CreateEntry("testEntry", loader);
	VFSVirtualEntry vEntry(entry, "testPath", "testName");
	EXPECT_EQ(vEntry.GetName(), entry->GetName());
}

TEST_F(VFSVirtualEntryTest, TestGetFullPath)
{
	VFSRoot root;
	auto loader = std::shared_ptr<VFSPathLoader>(new MockVFSPathLoader(root));
	auto entry = VFSEntryFactory::CreateEntry("testEntry", loader);
	VFSVirtualEntry vEntry(entry, "testPath", "testName");
	EXPECT_EQ(vEntry.GetFullPath(), entry->GetFullPath());
}

TEST_F(VFSVirtualEntryTest, TestGetLoader)
{
	VFSRoot root;
	auto loader = std::shared_ptr<VFSPathLoader>(new MockVFSPathLoader(root));
	auto entry = VFSEntryFactory::CreateEntry("testEntry", loader);
	VFSVirtualEntry vEntry(entry, "testPath", "testName");
	EXPECT_EQ(vEntry.GetLoader(), entry->GetLoader());
}