/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing a helper class for handling paths
 * 
 * @file VFSHelpers.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include <vector>
#include <string>

namespace oesvfs
{
	/**
	 * @brief Helper class to handle paths
	 * 
	 */
    class VFSHelpers
    {
    public:
		/**
		 * @brief Fix a path
		 * 
		 * @note Transforms a path to a path usable to the virtual file system
		 * 
		 * @param p_str Path to fix
		 */
		static void FixDirectoryPath(std::string &p_str);

		/**
		* @brief Replaces all occurences of a character with another
		*
		* @param p_str Path to modify
		* @param p_orig Original character
		* @param p_new New character
		*/
		static void ReplaceChar(std::string &p_str, const char p_orig = '\\', const char p_new = '/');

		/**
		 * @brief Checks if a string contains a character
		 * 
		 * @param p_str String to check
		 * @param p_chr Character to check
		 * @return true String contains at least one occurence of the character
		 * @return false Character is not present in the string
		 */
		static bool ContainsChar(const std::string& p_str, const char p_chr);

		/**
		* @brief Checks if a string contains a character
		*
		* @param p_str String to check
		* @param p_chr Character to check
		* @return true String contains at least one occurence of the character
		* @return false Character is not present in the string
		*/
		static bool ContainsChar(const std::string&& p_str, const char p_chr);

		/**
		 * @brief Get the root name
		 * 
		 * @param p_str Path
		 * @return std::string Root name
		 */
		static std::string GetRootName(const std::string &p_str);

		/**
		* @brief Get the root name
		*
		* @param p_str Path
		* @return std::string Root name
		*/
		static std::string GetRootName(const std::string &&p_str);

		/**
		* @brief Get the root directory
		*
		* @param p_str Path
		* @return std::string Root directory
		*/
		static std::string GetRootDir(const std::string &p_str);

		/**
		 * @brief Get the root directory
		 * 
		 * @param p_str Path
		 * @return std::string Root directory
		 */
		static std::string GetRootDir(const std::string &&p_str);

		/**
		* @brief Get the root path
		*
		* @param p_str Path
		* @return std::string Root path
		*/
		static std::string GetRootPath(const std::string &p_str);

		/**
		 * @brief Get the root path
		 * 
		 * @param p_str Path
		 * @return std::string Root path
		 */
		static std::string GetRootPath(const std::string &&p_str);

		/**
		* @brief Get the relative path
		*
		* @param p_str Path
		* @return std::string Relative path
		*/
		static std::string GetRelativePath(const std::string &p_str);

		/**
		 * @brief Get the relative path
		 * 
		 * @param p_str Path
		 * @return std::string Relative path
		 */
		static std::string GetRelativePath(const std::string &&p_str);

		/**
		* @brief Get the parent path
		*
		* @param p_str Path
		* @return std::string Parent path
		*/
		static std::string GetParentPath(const std::string &p_str);

		/**
		 * @brief Get the parent path
		 * 
		 * @param p_str Path
		 * @return std::string Parent path
		 */
		static std::string GetParentPath(const std::string &&p_str);

		/**
		* @brief Get the filename
		*
		* @param p_str Path
		* @return std::string Filename
		*/
		static std::string GetFilename(const std::string &p_str);

		/**
		 * @brief Get the filename
		 * 
		 * @param p_str Path
		 * @return std::string Filename
		 */
		static std::string GetFilename(const std::string &&p_str);

		/**
		* @brief Get the stem
		*
		* @param p_str Path
		* @return std::string Stem
		*/
		static std::string GetStem(const std::string &p_str);

		/**
		 * @brief Get the stem
		 * 
		 * @param p_str Path
		 * @return std::string Stem
		 */
		static std::string GetStem(const std::string &&p_str);

		/**
		* @brief Get the extension
		*
		* @param p_str Path
		* @return std::string Extension
		*/
		static std::string GetExtension(const std::string &p_str);

		/**
		 * @brief Get the extension
		 * 
		 * @param p_str Path
		 * @return std::string Extension
		 */
		static std::string GetExtension(const std::string &&p_str);

		/**
		* @brief Get the base directory
		*
		* @param p_str Path
		* @return std::string Base directory
		*/
		static std::string GetBaseDirectory(const std::string &p_str);

		/**
		 * @brief Get the base directory
		 * 
		 * @param p_str Path
		 * @return std::string Base directory
		 */
		static std::string GetBaseDirectory(const std::string &&p_str);

		/**
		* @brief Get the relative directory
		*
		* @param p_str Path
		* @return std::string Relative directory
		*/
		static std::string GetRelativeDirectory(const std::string &p_str);

		/**
		 * @brief Get the relative directory
		 * 
		 * @param p_str Path
		 * @return std::string Relative directory
		 */
		static std::string GetRelativeDirectory(const std::string &&p_str);

		/**
		 * @brief Get the current directory
		 * 
		 * @return std::string Current directory
		 */
		static std::string GetCurrentDirectory();
    };
}