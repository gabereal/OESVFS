 /*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing specialization of \ref VFSEntry for modelling Directories
 * 
 * @file VFSDirectory.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include "VFSEntry.h"

namespace oesvfs
{
    class VFSFile;

	/**
	 * @brief Class representing a directory
	 * 
	 * This class gives access to functionality and properties
	 * of a typical directory
	 * 
	 */
    class VFSDirectory : public VFSEntry
    {
    public:
		using entry_map = std::map<std::string, std::shared_ptr<VFSEntry>>;

        using iterator = entry_map::iterator;
        using const_iterator = entry_map::const_iterator;
        using reverse_iterator = entry_map::reverse_iterator;
        using const_reverse_iterator = entry_map::const_reverse_iterator;

		/**
		 * @brief Construct a new VFSDirectory object
		 * 
		 * @attention Throws \code{.cpp} std::invalid_argument \endcode in case the
		 * 			parent directory equals nullptr. See \ref VFSEntry::VFSEntry
		 * 
		 * @param p_parentDir Pointer to the parent directory
		 * @param p_name Name of this directory
		 * @param p_loader Loader responsible of loading this directory
		 */
        VFSDirectory(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader);

		/**
		 * @brief Destroy the VFSDirectory object
		 * 
		 */
        virtual ~VFSDirectory();

		/**
		 * @brief Get an entry
		 * 
		 * This method searches an entry in the entry tree.
		 * 
		 * @note If no entry has been found, it returns a nullptr
		 * @attention Mounted entries have precedence over other entries
		 * 
		 * @param p_path Search path
		 * @return std::shared_ptr<VFSEntry> Pointer to the entry
		 */
		virtual std::shared_ptr<VFSEntry> GetEntry(std::string p_path);

		/**
		 * @brief Get a directory
		 * 
		 * This method searches a directory in the entry tree.
		 * 
		 * @note Returns a nullptr if no entry has been found or the entry
		 * 		 if the entry is not a directory
		 * @attention Mounted entries have precedence over other entries
		 * 
		 * @param p_path Search path
		 * @return std::shared_ptr<VFSDirectory> Pointer to the directory
		 */
		virtual std::shared_ptr<VFSDirectory> GetDirectory(std::string p_path);

		/**
		 * @brief Get a file
		 * 
		 * This method searches a file in the entry tree.
		 * 
		 * @note Returns a nullptr if no entry has been found or the entry
		 * 		 if the entry is not a file
		 * @attention Mounted entries have precedence over other entries
		 * 
		 * @param p_path Search path
		 * @return std::shared_ptr<VFSFile> Pointer to the file
		 */
		virtual std::shared_ptr<VFSFile> GetFile(std::string p_path);

		
		using VFSEntry::CanRead;
		using VFSEntry::CanWrite;

		/**
		 * @brief Checks the entry can be read
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return bool Can be read
		 */
		virtual bool CanRead(std::string p_path);

		/**
		 * @brief Checks the entry can be written to
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return bool Can be written to
		 */
		virtual bool CanWrite(std::string p_path);

		using VFSEntry::CopyTo;
		using VFSEntry::FileSize;
		using VFSEntry::Remove;
		using VFSEntry::Rename;

		/**
		 * @brief Copy an entry to another location 
		 * 
		 * @note If p_destPath points to a file, it will be overwritten
		 * @note If p_destPath points to a directory, the entry will be
		 * 		 inserted into the directory, overwriting existing entries.
		 * @note Fails if the source entry can not be read from or if the 
		 * 		 destination entry can not be written to.
		 * @note Fails if the source or destination entry can not be found.
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_srcPath Search path of the entry to copy
		 * @param p_destPath Path of the destination
		 */
		virtual void Copy(std::string p_srcPath, std::string p_destPath);

		/**
		 * @brief Entry exists
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return true Entry exists
		 * @return false Entry was not found
		 */
		virtual bool Exists(std::string p_path);

		/**
		 * @brief Get size of an entry
		 * 
		 * @note Returns 0 if no entry was found
		 * @note If entry is a file, returns size of file
		 * @note If entry is a directory, returns sum of all real entries
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return std::uint64_t Size of the entry
		 */
		virtual std::uint64_t FileSize(std::string p_path);

		/**
		 * @brief Remove an entry
		 * 
		 * @note If a virtual entry has been found, only the real entry will be
		 *  	removed. Removing the virtual entry necessitates unmounting it.
		 * 		See \ref VFSRoot::Unmount and \ref VFSVirtualEntry::RemoveVirtualEntry
		 * 
		 * @note May fail if streams to entry are open
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return true Entry was removed
		 * @return false Entry could not be removed
		 */
		virtual bool Remove(std::string p_path);

		/**
		 * @brief Rename an entry
		 * 
		 * @note If a virtual entry has been found, only the real entry will be
		 *  	renamed. Renaming the virtual entry necessitates unmounting it.
		 * 		See \ref VFSRoot::Unmount and \ref VFSVirtualEntry::RemoveVirtualEntry
		 * 
		 * @note May fail if streams to entry are open
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_oldPath Path to the entry
		 * @param p_newName New name of the entry
		 * @return true Renaming successfull
		 * @return false Renaming not successfull
		 */
		virtual bool Rename(std::string p_oldPath, std::string p_newName);

		using VFSEntry::IsDirectory;
		using VFSEntry::IsFile;

		/**
		 * @brief Entry is a directory
		 * 
		 * @note Returns false if no entry was found. See \ref VFSEntry::IsDirectory
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return true Entry is a directory
		 * @return false Entry is not a directory
		 */
		virtual bool IsDirectory(std::string p_path);

		/**
		 * @brief Entry is a file
		 * 
		 * @note Returns false if no entry was found. See \ref VFSEntry::IsFile
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Search path
		 * @return true Entry is a file
		 * @return false Entry is not a file
		 */
		virtual bool IsFile(std::string p_path);

		/**
		 * @brief Opens a file
		 * 
		 * Opens a file like if it were a directory and inserts it into the
		 * parent directory as a virtual entry
		 * 
		 * @note If no file could be found, a nullptr will be returned
		 * @note If no suitable loader exists, a nullptr will be returned
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Path to the file
		 * @return std::shared_ptr<VFSDirectory> Pointer to the created Directory
		 */
		virtual std::shared_ptr<VFSDirectory> OpenFile(std::string p_path);

		/**
		 * @brief Create an empty directory
		 * 
		 * @note Fails if the path leading to the directory to create does not
		 * 		 exist. In that case a nullptr will be returned
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Path to the directory
		 * @return std::shared_ptr<VFSDirectory> Created directory
		 */
		virtual std::shared_ptr<VFSDirectory> CreateDirectory(std::string p_path);

		/**
		 * @brief Create an empty file
		 * 
		 * @note Fails if the path leading to the file to create does not
		 * 		 exist. In that case a nullptr will be returned
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Path to the file
		 * @return std::shared_ptr<VFSFile> Created file
		 */
		virtual std::shared_ptr<VFSFile> CreateFile(std::string p_path);

		/**
		 * @brief Create a file with the contents of the input
		 * 
		 * @note Fails if the path leading to the file to create does not
		 * 		 exist. In that case a nullptr will be returned
		 * 
		 * @attention See \ref GetEntry
		 * 
		 * @param p_path Path to the file
		 * @param p_input Input stream to be inserted into the file
		 * @return std::shared_ptr<VFSFile> Created file
		 */
		virtual std::shared_ptr<VFSFile> CreateFile(std::string p_path, std::istream& p_input);

		/**
		 * @brief Check if the directory contains an entry
		 * 
		 * Checks if the directory contains the entry as a direct child
		 * 
		 * @note Does not check virtual entries
		 * 
		 * @param p_entry Entry to check
		 * @return true Directory contains the entry
		 * @return false Directory does not contain the entry
		 */
		virtual bool IsOwnEntry(const std::shared_ptr<VFSEntry>& p_entry);

		/**
		 * @brief Get the Map object
		 * 
		 * Get the Map containing all entries of the directory.
		 * 
		 * @return VFSDirectory::entry_map& Map containing all entries
		 */
		virtual VFSDirectory::entry_map& GetMap();

		/**
		 * @brief Begin iterator
		 * 
		 * @return iterator Iterator pointing to the first entry
		 */
        iterator begin();

		/**
		 * @brief End iterator
		 * 
		 * @return iterator Iterator pointing past the last entry
		 */
		iterator end();

		/**
		 * @brief Constant begin iterator
		 * 
		 * @return const_iterator Constant iterator pointing to the first entry
		 */
		const_iterator begin() const;

		/**
		 * @brief Constant end iterator
		 * 
		 * @return const_iterator Constant iterator pointing past the last entry
		 */
		const_iterator end() const;

		/**
		 * @brief Reverse begin iterator
		 * 
		 * @return iterator Iterator pointing to the last entry
		 */
		reverse_iterator rbegin();

		/**
		 * @brief Reverse end iterator
		 * 
		 * @return iterator Iterator pointing past the first entry
		 */
		reverse_iterator rend();

		/**
		 * @brief Constant reverse begin iterator
		 * 
		 * @return iterator Constant iterator pointing to the last entry
		 */
		const_reverse_iterator rbegin() const;

		/**
		 * @brief Constant reverse end iterator
		 * 
		 * @return iterator Constant iterator pointing past the first entry
		 */
		const_reverse_iterator rend() const;

    protected:
		/**
		 * @brief Construct a new VFSDirectory object
		 * 
		 * @note Use this only in case of the directory being a root directory
		 * 		and having therefore no parent directory, as the public constructor
		 * 		requires a valid parent directory.
		 * 
		 * @param p_name Name of the directory
		 * @param p_loader Loader responsible to load the contents of the directory
		 */
		VFSDirectory(std::string p_name, std::shared_ptr<VFSPathLoader> p_loader);

		/**
		 * @brief Map containing all entries of the directory
		 */
		entry_map m_cachedEntries;

	private:
		/**
		* @brief Define \ref VFSRoot as a friend of this class, so that
		*		it can access the protected constructor
		*/
		friend class VFSRoot;
    };
}