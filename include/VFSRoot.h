/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include<vector>
#include<map>
#include<string>

#include "VFSEntry.h"
#include "VFSDirectory.h"
#include "VFSFile.h"
#include "VFSPathLoader.h"
#include "VFSHelpers.h"
#include "VFSVirtualEntry.h"

namespace oesvfs
{
    class VFSRoot
    {
    public:
        VFSRoot();
        ~VFSRoot();

		VFSRoot(const VFSRoot& other);
		VFSRoot(VFSRoot&& other) noexcept;

        void AddLoader(VFSPathLoader*& p_loader);
		void AddLoader(VFSPathLoader*&& p_loader);
        void Reset();

        void Mount(std::string p_path, std::string p_mountPoint);
        bool Unmount(std::string p_mountPoint);

		std::shared_ptr<VFSDirectory> OpenFile(std::string p_path);
		std::shared_ptr<VFSDirectory> CreateDirectory(std::string p_path);
		std::shared_ptr<VFSFile> CreateFile(std::string p_path);

        std::shared_ptr<VFSDirectory> GetDirectory(std::string p_path);
        std::shared_ptr<VFSFile> GetFile(std::string p_path);

        void Copy(std::string p_srcPath, std::string p_destPath);
        bool Exists(std::string p_path);
        std::uint64_t FileSize(std::string p_path);
        bool Remove(std::string p_path);
        bool Rename(std::string p_oldPath, std::string p_newPath);

        bool IsDirectory(std::string p_path);
        bool IsFile(std::string p_path);
		bool CanRead(std::string p_path);
		bool CanWrite(std::string p_path);

		const std::vector<std::shared_ptr<VFSPathLoader>>& GetLoaders();

		VFSRoot& operator=(const VFSRoot& other);
		VFSRoot& operator=(VFSRoot&& other) noexcept;

    private:
		std::shared_ptr<VFSDirectory> GetRootDirectory(std::string path);

        std::vector<std::shared_ptr<VFSPathLoader>> m_loaders;
		std::map<std::string, std::shared_ptr<VFSDirectory>> m_rootDirectories;
		std::map<std::string, std::shared_ptr<VFSVirtualEntry>> m_virtualEntries;
    };
}