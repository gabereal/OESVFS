/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing the base class of all entries.
 * 
 * @file VFSEntry.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include <vector>
#include <map>
#include <string>

namespace oesvfs
{
    class VFSPathLoader;
	class VFSVirtualEntry;
	class VFSDirectory;

    /**
     * @brief Class containing common functionality of all entries
     */
    class VFSEntry : public std::enable_shared_from_this<VFSEntry>
    {
    public:
        /**
         * @brief Construct a new VFSEntry object
         * 
         * @note Throws an invalid_argument exception, if p_parentDir is nullptr
         * 
         * @param p_parentDir Pointer to the parent directory
         * @param p_name Name of the entry
         * @param p_loader Pointer to the loader responsible of loading this entry
         */
        VFSEntry(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader);

        /**
         * @brief Destroy the VFSEntry object
         */
        virtual ~VFSEntry();

        /**
         * @brief Get the name of this entry
         * 
         * @return std::string Name of this entry
         */
        std::string GetName();

        /**
         * @brief Get the full path of this entry
         * 
         * @note The Path is composed of the name of this entry with, in case a parent directory exists,
         *      the full path of the parent directory and a '/' prepended to it.
         * 
         * @return std::string Full path
         */
        std::string GetFullPath();

        /**
         * @brief Get the Loader
         * 
         * @return std::shared_ptr<VFSPathLoader> Loader
         */
        std::shared_ptr<VFSPathLoader> GetLoader();

        /**
         * @brief Get the parent directory
         * 
         * @return VFSDirectory* Parent
         */
		VFSDirectory* GetParent();

        /**
         * @brief Remove a virtual entry
         * 
         * @note See \ref VFSVirtualEntry::RemoveVirtualEntry
         * @note A new virtual entry will be created if this 
         *      entry possesses no virtual entry.
         * 
         * @param p_name Name of the virtual entry
         */
        void RemoveVirtualEntry(std::string p_name);

        /**
         * @brief Get the virtual entry of this entry
         * 
         * @note A new virtual entry will be created if this 
         *      entry possesses no virtual entry.
         * 
         * @note Never returns a nullptr.
         * 
         * @return std::shared_ptr<VFSVirtualEntry> Virtual entry
         */
        std::shared_ptr<VFSVirtualEntry> GetVirtualEntry();

        /**
         * @brief Get a virtual entry
         * 
         * @note See \ref VFSVirtualEntry::GetVirtualEntry
         * @note A new virtual entry will be created if this 
         *      entry possesses no virtual entry.
         * 
         * @param p_name Name of the virtual entry
         * @return std::shared_ptr<VFSVirtualEntry> Virtual entry
         */
        std::shared_ptr<VFSVirtualEntry> GetVirtualEntry(std::string p_name);

        /**
         * @brief Add a virtual entry
         * 
         * @note See \ref VFSVirtualEntry::AddVirtualEntry
         * @note A new virtual entry will be created if this 
         *      entry possesses no virtual entry.
         * 
         * @param p_entry 
         * @param p_virtualName 
         * @return std::shared_ptr<VFSVirtualEntry> 
         */
        std::shared_ptr<VFSVirtualEntry> AddVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName);

        /**
         * @brief Return a shared_ptr of this entry
         * 
         * @return std::shared_ptr<VFSEntry> Pointer
         */
		std::shared_ptr<VFSEntry> GetThis();

        /**
         * @brief Loads this entry
         * 
         * @note See \ref VFSPathLoader::Load
         */
        void Load();

        /**
         * @brief Copy this entry
         * 
         * @note See \ref VFSPathLoader::Copy
         * 
         * @param p_dest Where this entry will be copied to
         */
        void CopyTo(const std::shared_ptr<VFSEntry> p_dest);

        /**
         * @brief Get size of this entry
         * 
         * @note See \ref VFSPathLoader::Size
         * 
         * @return std::uint64_t Size
         */
        std::uint64_t FileSize();

        /**
         * @brief Remove this entry
         * 
         * @note See \ref VFSPathLoader::Remove
         * 
         * @return true Removal successfull
         * @return false Removal not successfull
         */
        bool Remove();

        /**
         * @brief Rename this entry
         * 
         * @note See \ref VFSPathLoader::Rename
         * 
         * @param p_newName New name
         * @return true Renaming successfull
         * @return false Renaming not successfull
         */
        bool Rename(std::string p_newName);

        /**
         * @brief Is this entry a directory
         * 
         * @return true Is a directory
         * @return false Is not a directory
         */
        bool IsDirectory();

        /**
         * @brief Is this entry a file
         * 
         * @return true Is a file
         * @return false Is not a file
         */
        bool IsFile();

        /**
         * @brief Has this entry been loaded
         * 
         * @return true Entry was loaded
         * @return false Entry was not loaded
         */
		bool IsLoaded();

        /**
         * @brief Returns if entry be read from
         * 
         * @return true Entry can be read
         * @return false Entry can not be read
         */
        bool CanRead();

        /**
         * @brief Returns if entry can be written to
         * 
         * @return true Entry can be written
         * @return false Entry can not be written
         */
        bool CanWrite();

    protected:
        /**
         * @brief Construct a new VFSEntry object
         * 
         * @param p_name Name of the entry
         * @param p_loader Loader responsible of loading the entry
         */
		VFSEntry(std::string p_name, std::shared_ptr<VFSPathLoader> p_loader);

        /**
         * @brief Loader responsible of loading the entry
         */
        std::shared_ptr<VFSPathLoader> m_loader;

        /**
         * @brief Virtual entry of the entry
         */
		std::shared_ptr<VFSVirtualEntry> m_virtualEntry;

        /**
         * @brief Parent directory of the entry
         */
		VFSDirectory* m_parentDir;

        /**
         * @brief If the entry is loaded
         */
		bool m_loaded;

        /**
         * @brief Name of the entry
         */
		std::string m_name;
    };
}