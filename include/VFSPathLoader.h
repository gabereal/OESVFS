/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing the Interface of a loader
 * 
 * @file VFSPathLoader.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include<memory>
#include<string>
#include<map>

namespace oesvfs
{
	class VFSRoot;
    class VFSEntry;
	class VFSFile;
	class VFSDirectory;

	/**
	 * @brief Interface describing a loader
	 */
    class VFSPathLoader : public std::enable_shared_from_this<VFSPathLoader>
    {
    public:
		/**
		 * @brief Construct a new VFSPathLoader object
		 * 
		 * @param p_root Root object
		 */
		VFSPathLoader(VFSRoot& p_root);

		/**
		 * @brief Destroy the VFSPathLoader object
		 */
        virtual ~VFSPathLoader() = default;

		/**
		 * @brief Get a shared_ptr of this object
		 * 
		 * @return std::shared_ptr<VFSPathLoader> Pointer of this object 
		 */
		std::shared_ptr<VFSPathLoader> GetThis();

		/**
		 * @brief Checks if an entry can be loaded
		 * 
		 * @note Calls the overloaded method depending whether
		 * 		the passed entry is a directory or a file
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry can be loaded
		 * @return false Entry can not be loaded
		 */
		virtual bool CanLoad(VFSEntry& p_entry);

		/**
		 * @brief Checks if a directory can be loaded
		 * 
		 * @param p_dir Directory to check
		 * @return true Directory can be loaded
		 * @return false Directory can not be loaded
		 */
		virtual bool CanLoad(VFSDirectory& p_dir) = 0;

		/**
		 * @brief Checks if a file can be loaded
		 * 
		 * @param p_file File to check
		 * @return true File can be loaded
		 * @return false File can not be loaded
		 */
		virtual bool CanLoad(VFSFile& p_file) = 0;

		/**
		 * @brief Checks if this loader can access a path
		 * 
		 * @param p_path Path to check
		 * @return true Path can be accessed
		 * @return false Path can not be accessed
		 */
		virtual bool CanReadPath(std::string p_path) = 0;

		/**
		 * @brief Opens a file as a directory
		 * 
		 * @note Calls \ref CanLoad to check if the file can be loaded.
		 * 		The method \ref _OpenFile get called in case the file
		 * 		can be loaded. Otherwise a nullptr is returned
		 * 
		 * @param p_file File to open
		 * @return std::shared_ptr<VFSDirectory> Pointer to the new directory
		 */
		virtual std::shared_ptr<VFSDirectory> OpenFile(VFSFile& p_file);

		/**
		 * @brief Create a new directory
		 * 
		 * @param p_rootEntry Parent directory
		 * @param p_path Name of the new directory
		 * @return std::shared_ptr<VFSDirectory> Pointer to the new directory
		 */
		virtual std::shared_ptr<VFSDirectory> CreateDirectory(VFSDirectory& p_rootEntry, std::string p_path);
		
		/**
		 * @brief Create a new empty file
		 * 
		 * @param p_rootEntry Parent directory
		 * @param p_path Name of the new file
		 * @return std::shared_ptr<VFSFile> Pointer to the new file
		 */
		virtual std::shared_ptr<VFSFile> CreateFile(VFSDirectory& p_rootEntry, std::string p_path);

		/**
		 * @brief Create a File object
		 * 
		 * @param p_rootEntry 
		 * @param p_path 
		 * @param p_input 
		 * @return std::shared_ptr<VFSFile> 
		 */
		virtual std::shared_ptr<VFSFile> CreateFile(VFSDirectory& p_rootEntry, std::string p_path, std::istream& p_input) = 0;

		/**
		 * @brief Load a file
		 * 
		 * @param p_file File to load
		 * @return true File has been loaded
		 * @return false File could not be loaded
		 */
		virtual bool Load(VFSFile& p_file);

		/**
		 * @brief Loaad a directory
		 * 
		 * @param p_directory Directory to load
		 * @return true Directory has been loaded
		 * @return false Directory could not be loaded
		 */
		virtual bool Load(VFSDirectory& p_directory);

		/**
		 * @brief Copy an entry to a destination
		 * 
		 * @param p_entry Entry to copy
		 * @param p_dest Destination
		 */
		virtual void Copy(VFSEntry& p_entry, std::shared_ptr<VFSEntry> p_dest);

		/**
		 * @brief Copy a file to a destination
		 * 
		 * @param p_file File to copy
		 * @param p_dest Destination
		 */
		virtual void Copy(VFSFile& p_file, std::shared_ptr<VFSEntry> p_dest);

		/**
		 * @brief Copy a directory to a destination
		 * 
		 * @param p_dir Directory
		 * @param p_dest Destination
		 */
		virtual void Copy(VFSDirectory& p_dir, std::shared_ptr<VFSEntry> p_dest);

		/**
		 * @brief Get the size of an entry
		 * 
		 * @param p_entry Entry to check
		 * @return std::uint64_t Size of the entry
		 */
		virtual std::uint64_t Size(VFSEntry& p_entry);

		/**
		 * @brief Remove an entry
		 * 
		 * @param p_parent Parent directory
		 * @param p_entry Entry to remove
		 * @return true Entry has been removed
		 * @return false Entry could not be removed
		 */
		virtual bool Remove(VFSDirectory& p_parent, VFSEntry& p_entry);

		/**
		 * @brief Rename an entry
		 * 
		 * @param p_parent Parent directory
		 * @param p_entry Entry to rename
		 * @param p_newName New name
		 * @return true Entry has been renamed
		 * @return false Entry could not be renamed
		 */
		virtual bool Rename(VFSDirectory& p_parent, VFSEntry& p_entry, std::string p_newName) = 0;

		/**
		 * @brief Check if entry is a directory
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry is a directory
		 * @return false Entry is not a directory
		 */
		virtual bool IsDirectory(VFSEntry& p_entry) = 0;

		/**
		 * @brief Check if entry is a file
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry is a file
		 * @return false Entry is not a file
		 */
		virtual bool IsFile(VFSEntry& p_entry) = 0;

		/**
		 * @brief Check if the entry can be read
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry can be read from
		 * @return false Entry can not be read
		 */
		virtual bool CanRead(VFSEntry& p_entry) = 0;

		/**
		 * @brief Check if the entry can be written to
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry can be written to
		 * @return false Entry can not be written to
		 */
		virtual bool CanWrite(VFSEntry& p_entry) = 0;

		/**
		 * @brief Get the input stream
		 * 
		 * @param p_entry File to open
		 * @param p_mode How to open the file
		 * @return std::shared_ptr<std::istream> Pointer to input stream
		 */
		virtual std::shared_ptr<std::istream> GetInputStream(VFSFile& p_entry, std::ios_base::openmode p_mode = std::ios_base::in | std::ios_base::binary) = 0;

		/**
		 * @brief Get the output stream
		 * 
		 * @param p_entry File to open
		 * @param p_mode How to open the file
		 * @return std::shared_ptr<std::ostream> Pointer to the output stream
		 */
		virtual std::shared_ptr<std::ostream> GetOutputStream(VFSFile& p_entry, std::ios_base::openmode p_mode = std::ios_base::out | std::ios_base::binary) = 0;

		/**
		 * @brief Close an input stream
		 * 
		 * @param p_file Original file
		 * @param p_stream Stream to close
		 */
		virtual void CloseStream(VFSFile& p_file, std::shared_ptr<std::istream>& p_stream) = 0;

		/**
		 * @brief Close an output stream
		 * 
		 * @param p_file Original file
		 * @param p_stream Stream to close
		 */
		virtual void CloseStream(VFSFile& p_file, std::shared_ptr<std::ostream>& p_stream) = 0;

	protected:
		/**
		 * @brief Open a file as a directory
		 * 
		 * @param p_file File to open
		 * @return std::shared_ptr<VFSDirectory> New directory
		 */
		virtual std::shared_ptr<VFSDirectory> OpenFile_(VFSFile& p_file) = 0;

		/**
		 * @brief Create a new directory
		 * 
		 * @param p_rootEntry Parent directory
		 * @param p_relativePath Name of the directory
		 * @return std::shared_ptr<VFSDirectory> New directory
		 */
		virtual std::shared_ptr<VFSDirectory> CreateDirectory_(VFSDirectory& p_rootEntry, std::string p_relativePath) = 0;

		/**
		 * @brief Create a new empty file
		 * 
		 * @param p_rootEntry Parent directory
		 * @param p_relativePath Name of the file
		 * @return std::shared_ptr<VFSFile> New file
		 */
		virtual std::shared_ptr<VFSFile> CreateFile_(VFSDirectory& p_rootEntry, std::string p_relativePath) = 0;

		/**
		 * @brief Load a file
		 * 
		 * @param p_entry File to load
		 * @return true File has been loaded
		 * @return false File could not be loaded
		 */
		virtual bool Load_(VFSFile& p_entry) = 0;

		/**
		 * @brief Load a directory
		 * 
		 * @param p_directory Directory to load
		 * @return true Directory has been loaded
		 * @return false Directory could not be loaded
		 */
		virtual bool Load_(VFSDirectory& p_directory) = 0;

		/**
		 * @brief Copy a file to another file
		 * 
		 * @param p_entry Source file
		 * @param p_dest File to overwrite
		 */
		virtual void CopyFile_(VFSFile& p_entry, std::shared_ptr<VFSFile> p_dest) = 0;

		/**
		 * @brief Copy a file to another directory
		 * 
		 * @param p_entry Source file
		 * @param p_dest Destination directory
		 */
		virtual void CopyFile_(VFSFile& p_entry, std::shared_ptr<VFSDirectory> p_dest) = 0;

		/**
		 * @brief Copy a directory to another directory
		 * 
		 * @param p_entry Source directory
		 * @param p_dest Destination directory
		 */
		virtual void CopyDirectory_(VFSDirectory& p_entry, std::shared_ptr<VFSDirectory> p_dest) = 0;

		/**
		 * @brief Get size of a directory
		 * 
		 * @param p_entry Directory to check
		 * @return std::uint64_t Size of the directory
		 */
		virtual std::uint64_t Size_(VFSDirectory& p_entry);

		/**
		 * @brief Get size of a file
		 * 
		 * @param p_entry File to check
		 * @return std::uint64_t Size of the file
		 */
		virtual std::uint64_t Size_(VFSFile& p_entry) = 0;

		/**
		 * @brief Remove a directory
		 * 
		 * @param p_parent Parent directory
		 * @param p_entry Directory to remove
		 * @return true Directory has been removed
		 * @return false Directory could not be removed
		 */
		virtual bool Remove_(VFSDirectory& p_parent, VFSDirectory& p_entry) = 0;

		/**
		 * @brief Remove a file
		 * 
		 * @param p_parent Parent directory
		 * @param p_entry File to remove
		 * @return true File has been removed
		 * @return false File could not be removed
		 */
		virtual bool Remove_(VFSDirectory& p_parent, VFSFile& p_entry) = 0;

		/**
		 * @brief Find loader able to load the entry
		 * 
		 * @param p_dir Entry to check
		 * @return std::shared_ptr<VFSPathLoader> Pointer to loader
		 */
		std::shared_ptr<VFSPathLoader> FindSuitableLoader(VFSEntry& p_dir);

		/**
		 * @brief Root object
		 */
		VFSRoot & m_root;
    };
}