/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing the base class of all virtual entries
 * 
 * @file VFSVirtualEntry.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include "VFSEntry.h"

namespace oesvfs
{
	/**
	 * @brief Class describing a virtual entry
	 * 
	 */
	class VFSVirtualEntry
	{
	public:
		/**
		 * @brief Construct a new VFSVirtualEntry object
		 * 
		 * @param p_entry Real entry
		 * @param p_virtualName Name of the virtual entry
		 */
		VFSVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName);

		/**
		 * @brief Construct a new VFSVirtualEntry object
		 * 
		 * @param p_entry Real entry
		 * @param p_fullVirtualPath Virtual path
		 * @param p_virtualName Name of the virtual entry
		 */
		VFSVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_fullVirtualPath, std::string p_virtualName);

		/**
		 * @brief Destroy the VFSVirtualEntry object
		 */
		virtual ~VFSVirtualEntry();

		/**
		 * @brief Set the entry of this virtual entry
		 * 
		 * @param p_entry New entry
		 */
		void SetEntry(std::shared_ptr<VFSEntry> p_entry);

		/**
		 * @brief Get the entry
		 * 
		 * @return std::shared_ptr<VFSEntry> Entry
		 */
		std::shared_ptr<VFSEntry> GetEntry();

		/**
		 * @brief Get the name of the entry
		 * 
		 * @return std::string Name of the entry
		 */
		std::string GetName();

		/**
		 * @brief Get the full path of the entry
		 * 
		 * @return std::string Full path
		 */
		std::string GetFullPath();

		/**
		 * @brief Get the loader of the entry
		 * 
		 * @return std::shared_ptr<VFSPathLoader> Loader
		 */
		std::shared_ptr<VFSPathLoader> GetLoader();

		/**
		 * @brief Remove a virtual entry
		 * 
		 * @param p_name Name of the virtual entry
		 */
		void RemoveVirtualEntry(std::string p_name);

		/**
		 * @brief Get a virtual entry
		 * 
		 * @param p_name Name of the virtual entry
		 * @return std::shared_ptr<VFSVirtualEntry> Virtual entry
		 */
		std::shared_ptr<VFSVirtualEntry> GetVirtualEntry(std::string p_name);

		/**
		 * @brief Add a virtual entry to this virtual entry
		 * 
		 * @param p_entry Real entry
		 * @param p_virtualName Name of the virtual entry
		 * @return std::shared_ptr<VFSVirtualEntry> Added virtual entry
		 */
		std::shared_ptr<VFSVirtualEntry> AddVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName);

		/**
		 * @brief Get the name of the virtual entry
		 * 
		 * @return std::string Name of the virtual entry
		 */
		std::string GetVirtualName();

		/**
		 * @brief Get the full path of the virtual entry
		 * 
		 * @return std::string Full path of the virtual entry
		 */
		std::string GetFullVirtualPath();
		
		/**
		 * @brief Load entry
		 */
		void Load();

		/**
		 * @brief Copy entry
		 * 
		 * @param p_dest Destination entry
		 */
		void CopyTo(const std::shared_ptr<VFSVirtualEntry> p_dest);

		/**
		 * @brief Get the size of the entry
		 * 
		 * @return std::uint64_t Size of the entry
		 */
		std::uint64_t FileSize();

		/**
		 * @brief Remove entry
		 * 
		 * @return true Entry has been removed
		 * @return false Entry could not be removed
		 */
		bool Remove();

		/**
		 * @brief Rename the entry
		 * 
		 * @param p_newName New name of the entry
		 * @return true Entry was renamed
		 * @return false Entry could not be renamed
		 */
		bool Rename(std::string p_newName);

		/**
		 * @brief Reset the virtual entry
		 * 
		 */
		void Reset();

		/**
		 * @brief Checks if the entry exists
		 * 
		 * @return true Entry exists
		 * @return false Entry does not exist
		 */
		bool Exists();

		/**
		 * @brief Is the entry a directory
		 * 
		 * @return true Entry is a directory
		 * @return false Entry is not a directory
		 */
		bool IsDirectory();

		/**
		 * @brief Is the entry a file
		 * 
		 * @return true Entry is a file
		 * @return false Entry is not a file
		 */
		bool IsFile();

		/**
		 * @brief Can the entry be read
		 * 
		 * @return true Entry can be read
		 * @return false Entry can not be read
		 */
		bool CanRead();

		/**
		 * @brief Can the entry be written to
		 * 
		 * @return true Entry can be written to
		 * @return false Entry can not be written to
		 */
		bool CanWrite();

	protected:
		/**
		 * @brief Real entry of the virtual entry
		 */
		std::shared_ptr<VFSEntry> m_entry;

		/**
		 * @brief Full path of the virtual entry
		 */
		std::string m_fullVirtualPath;

		/**
		 * @brief Name of the virtual entry
		 */
		std::string m_virtualName;

		/**
		 * @brief Map of all the virtual entries of this virtual entry
		 */
		std::map<std::string, std::shared_ptr<VFSVirtualEntry>> m_virtualEntries;
	};
}