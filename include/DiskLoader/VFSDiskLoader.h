/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing specialization of \ref VFSPathLoader
 * 
 * @file VFSDiskLoader.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include "VFSPathLoader.h"

namespace oesvfs
{
	/**
	 * @brief Specialization of \ref VFSPathLoader
	 * 
	 * Enables loading of the native filesystem
	 * 
	 */
	class VFSDiskLoader : public VFSPathLoader
	{
	public:
		/**
		 * @brief Construct a new VFSDiskLoader object
		 * 
		 * @param p_root Reference to the root object
		 */
		VFSDiskLoader(VFSRoot& p_root);

		/**
		 * @brief Destroy the VFSDiskLoader object
		 * 
		 */
		virtual ~VFSDiskLoader() = default;

		/**
		 * @brief Checks if a directory can be loaded
		 * 
		 * @param p_dir Directory to check
		 * @return true Directory can be loaded
		 * @return false Directory can not be loaded
		 */
		bool CanLoad(VFSDirectory& p_dir) override;

		/**
		 * @brief Checks if a file can be loaded
		 * 
		 * @note Should return false, as this loader can only load directories
		 * 
		 * @param p_file File to check
		 * @return true File can be loaded
		 * @return false File can not be loaded
		 */
		bool CanLoad(VFSFile& p_file) override;

		/**
		 * @brief Checks if the path can be accessed
		 * 
		 * @param p_path Path to check
		 * @return true Path can be accessed
		 * @return false Path can not be accessed
		 */
		bool CanReadPath(std::string p_path) override;

		/**
		 * @brief Opens a file as a directory
		 * 
		 * @note Returns nullptr as this loader is not able to load files
		 * 
		 * @param p_file File to open
		 * @return std::shared_ptr<VFSDirectory> Opened directory
		 */
		std::shared_ptr<VFSDirectory> OpenFile(VFSFile& p_file) override;

		/**
		 * @brief Create a new File
		 * 
		 * @param p_rootEntry Directory in which to insert the file
		 * @param p_relativePath Name of the file
		 * @param p_input What to write into the file
		 * @return std::shared_ptr<VFSFile> Pointer to newly created file
		 */
		std::shared_ptr<VFSFile> CreateFile(VFSDirectory& p_rootEntry, std::string p_relativePath, std::istream& p_input) override;

		/**
		 * @brief Rename an entry
		 * 
		 * @note Fails if p_entry has open streams
		 * 
		 * @param p_parent Directory containing the entry
		 * @param p_entry Entry to rename
		 * @param p_newName New name of the entry
		 * @return true Renaming was successfull
		 * @return false Renaming failed
		 */
		bool Rename(VFSDirectory& p_parent, VFSEntry& p_entry, std::string p_newName) override;

		/**
		 * @brief Checks if an entry is a directory
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry is a directory
		 * @return false Entry is not a directory
		 */
		bool IsDirectory(VFSEntry& p_entry) override;

		/**
		 * @brief Checks if an entry is a file
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry is a file
		 * @return false Entry is not a file
		 */
		bool IsFile(VFSEntry& p_entry) override;

		/**
		 * @brief Checks if entry can be read from
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry can be read from
		 * @return false No read access available
		 */
		bool CanRead(VFSEntry& p_entry) override;

		/**
		 * @brief Checks if entry can be written to
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry can be written to
		 * @return false No write access available
		 */
		bool CanWrite(VFSEntry& p_entry) override;

		/**
		 * @brief Get the Input Stream object
		 * 
		 * @note Calling this method causes the creation of a new stream
		 * @note Defaults to opening the stream with the parameters in and binary
		 * 
		 * @param p_entry Source file
		 * @param p_mode How to open the stream
		 * @return std::shared_ptr<std::istream> Pointer to newly created input stream
		 */
		std::shared_ptr<std::istream> GetInputStream(VFSFile& p_entry, std::ios_base::openmode p_mode = std::ios_base::in | std::ios_base::binary) override;

		/**
		 * @brief Get the Output Stream object
		 * 
		 * @note Calling this method causes the creation of a new stream
		 * @note Defaults to opening the stream with the parameters out and binary
		 * 
		 * @param p_entry Destination file
		 * @param p_mode How to open the stream
		 * @return std::shared_ptr<std::ostream> Pointer to newly created output stream
		 */
		std::shared_ptr<std::ostream> GetOutputStream(VFSFile &p_entry, std::ios_base::openmode p_mode = std::ios_base::out | std::ios_base::binary) override;

		/**
		 * @brief Close an input stream
		 * 
		 * @param p_file Where the stream originates from
		 * @param p_stream Stream to close
		 */
		void CloseStream(VFSFile& p_file, std::shared_ptr<std::istream>& p_stream) override;

		/**
		 * @brief Close an output stream
		 * 
		 * @param p_file Where the stream originates from
		 * @param p_stream Stream to close
		 */
		void CloseStream(VFSFile& p_file, std::shared_ptr<std::ostream>& p_stream) override;

	protected:
		/**
		 * @brief Open a file
		 * 
		 * @note Internal method. See \ref OpenFile
		 * 
		 * @param p_file File to open
		 * @return std::shared_ptr<VFSDirectory> Opened directory
		 */
		virtual std::shared_ptr<VFSDirectory> OpenFile_(VFSFile& p_file) override;

		/**
		 * @brief Create a new directory
		 * 
		 * @note Internal method. See \ref VFSPathLoader::CreateDirectory
		 * @attention Creating an already existing directory results in an override
		 * 
		 * @param p_rootEntry Directory where to create the new directory
		 * @param p_relativePath Name of the new directory
		 * @return std::shared_ptr<VFSDirectory> Pointer to the newly created directory
		 */
		std::shared_ptr<VFSDirectory> CreateDirectory_(VFSDirectory& p_rootEntry, std::string p_relativePath) override;

		/**
		 * @brief Create a new empty file
		 * 
		 * @note Internal method. See \ref VFSPathLoader::CreateFile
		 * @attention Creating an already existing file results in an override
		 * 
		 * @param p_rootEntry Directory where to create the new file
		 * @param p_relativePath Name of the new file
		 * @return std::shared_ptr<VFSFile> Pointer to the newly created file
		 */
		std::shared_ptr<VFSFile> CreateFile_(VFSDirectory& p_rootEntry, std::string p_relativePath) override;

		/**
		 * @brief Loads a file
		 * 
		 * @note Internal method. See \ref VFSPathLoader::Load
		 * @note Returns false as this loader is unable to load files
		 * 
		 * @param p_file File to be loaded
		 * @return true File has been loaded
		 * @return false File could not be loaded
		 */
		bool Load_(VFSFile& p_file) override;

		/**
		 * @brief Loads a directory
		 * 
		 * @note Internal method. See \ref VFSPathLoader::Load
		 * @note Checks if all real entries of the directory exist.
		 * 		If this is not the case, they will be removed.
		 * 
		 * @param p_dir Directory to load
		 * @return true Directory has been loaded
		 * @return false Directory could not be loaded
		 */
		bool Load_(VFSDirectory& p_dir) override;

		/**
		 * @brief Copy contents of a file to another
		 * 
		 * @note Internal method. See \ref VFSPathLoader::CopyFile
		 * 
		 * @param p_entry Source file
		 * @param p_dest File to overwrite
		 */
		void CopyFile_(VFSFile& p_entry, std::shared_ptr<VFSFile> p_dest) override;

		/**
		 * @brief Create a new clone of a file
		 * 
		 * @note Internal method. See \ref VFSPathLoader::CopyFile
		 * @note Overwrites existing entries with the same name as the source file.
		 * 
		 * @param p_entry Source file
		 * @param p_dest Destination directory
		 */
		void CopyFile_(VFSFile& p_entry, std::shared_ptr<VFSDirectory> p_dest) override;

		/**
		 * @brief Copy directory to another directory
		 * 
		 * Creates a new directory with the same name as the source in the destination directory
		 * and then copies all entries of the source directory recursively into the newly created
		 * directory.
		 * 
		 * @note Internal method. See \ref VFSPathLoader::CopyDirectory
		 * @note Overwrites existing entries with the same name as the source file.
		 * 
		 * @param p_entry Source directory
		 * @param p_dest Destination directory
		 */
		void CopyDirectory_(VFSDirectory& p_entry, std::shared_ptr<VFSDirectory> p_dest) override;

		/**
		 * @brief Gets the size of a file
		 * 
		 * @note Internal method. See \ref VFSPathLoader::Size
		 * 
		 * @param p_file File to check
		 * @return std::uint64_t Size of the file
		 */
		std::uint64_t Size_(VFSFile& p_file) override;

		/**
		 * @brief Remove a directory
		 * 
		 * @note Internal method. See \ref VFSPathLoader::Remove
		 * @note Fails if directory contains entries with open streams
		 * 
		 * @param p_parent Directory in which the directory resides
		 * @param p_dir Directory to remove
		 * @return true Directory has been removed
		 * @return false Directory could not be removed
		 */
		bool Remove_(VFSDirectory& p_parent, VFSDirectory& p_dir) override;

		/**
		 * @brief Remove a file
		 * 
		 * @note Internal method. See \ref VFSPathLoader::Remove
		 * @note Fails if file has open streams
		 * 
		 * @param p_parent Directory in which the file resides
		 * @param p_file File to remove
		 * @return true File has been removed
		 * @return false File could not be removed
		 */
		bool Remove_(VFSDirectory& p_parent, VFSFile& p_file) override;

		/**
		 * @brief Checks if an entry has open streams
		 * 
		 * If the entry is a file or a directory, the respective methods will
		 * be called. If this is not the case, this method returns false
		 * 
		 * @param p_entry Entry to check
		 * @return true Entry has open streams
		 * @return false Entry has no open streams
		 */
		bool HasOpenStreams(VFSEntry& p_entry);

		/**
		 * @brief Checks if a file has open streams
		 * 
		 * @param p_file File to check
		 * @return true File has open streams
		 * @return false File has no open streams
		 */
		bool HasOpenStreams(VFSFile& p_file);

		/**
		 * @brief Check if a directory has open streams
		 * 
		 * Checks recursively if any of its entries has any open streams
		 * 
		 * @param p_dir Directory to check
		 * @return true Directory has open streams
		 * @return false Directory has no open streams
		 */
		bool HasOpenStreams(VFSDirectory& p_dir);

		/**
		 * @brief Map containing the number of streams a given entry has open
		 * 
		 * @note Uses the path of the entry as the key
		 */
		std::map<std::string, std::uint64_t> m_openStreams;
	};
}