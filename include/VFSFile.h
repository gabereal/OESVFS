/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

/**
 * @brief File containing specialization of \ref VFSEntry for modelling Files
 * 
 * @file VFSFile.h
 * @author GabeReal
 * @date 2018
 */

#pragma once
#include "VFSEntry.h"

namespace oesvfs
{
    /**
	 * @brief Class representing a file
	 * 
	 * This class gives access to functionality and properties
	 * of a typical file
	 * 
	 */
    class VFSFile : public VFSEntry
    {
    public:
        /**
         * @brief Construct a new VFSFile object
         * 
         * @attention Throws \code{.cpp} std::invalid_argument \endcode in case the
		 * 			parent directory equals nullptr. See \ref VFSEntry::VFSEntry
         * 
         * @param p_parentDir Parent directory
         * @param p_name Name of the file
         * @param p_loader Loader responsible of loading the file
         */
        VFSFile(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader);

        /**
         * @brief Destroy the VFSFile object
         */
        virtual ~VFSFile() = default;

        /**
         * @brief Open the file as a directory
         * 
         * @note See \ref VFSPathLoader::Load
         * 
         * @return std::shared_ptr<VFSDirectory> New directory
         */
		std::shared_ptr<VFSDirectory> Open();

        /**
         * @brief Get the Input Stream object
         * 
         * @param p_mode How to open the file
         * @return std::shared_ptr<std::istream> Input stream
         */
		std::shared_ptr<std::istream> GetInputStream(std::ios_base::openmode p_mode = std::ios_base::in | std::ios_base::binary);

        /**
         * @brief Get the Output Stream object
         * 
         * @param p_mode How to open the file
         * @return std::shared_ptr<std::ostream> Output stream
         */
        std::shared_ptr<std::ostream> GetOutputStream(std::ios_base::openmode p_mode = std::ios_base::out | std::ios_base::binary);

        /**
         * @brief Close an input stream
         * 
         * @param p_stream Input stream
         */
		void CloseStream(std::shared_ptr<std::istream>& p_stream);

        /**
         * @brief Close an output stream
         * 
         * @param p_stream Output stream
         */
		void CloseStream(std::shared_ptr<std::ostream>& p_stream);
    };
}