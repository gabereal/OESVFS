#include "VFSRoot.h"
#include "DiskLoader/VFSDiskLoader.h"

using namespace oesvfs;

int main()
{
	VFSRoot root;
	root.AddLoader(new VFSDiskLoader(root));
	auto dir = root.GetDirectory("D:\\Dokumente\\Projekte\\C++\\OESVFS\\example");

	auto file = dir->CreateFile("a.txt");
	auto stream = file->GetOutputStream();
	const char* text = "Hello File";
	stream->write(text, strlen(text));
	file->CloseStream(stream);

	auto subDir = dir->CreateDirectory("test");
	file->CopyTo(subDir);
	file->Rename("b.txt");

	return 0;
}