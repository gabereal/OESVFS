/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSFile.h"
#include "include/VFSPathLoader.h"

namespace oesvfs
{
	VFSFile::VFSFile(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader) : VFSEntry(p_parentDir, p_name, p_loader)
	{
	}

	std::shared_ptr<VFSDirectory> VFSFile::Open()
	{
		return m_loader->OpenFile(*this);
	}

	std::shared_ptr<std::istream> VFSFile::GetInputStream(std::ios_base::openmode p_mode)
	{
		return m_loader->GetInputStream(*this, p_mode);
	}

	std::shared_ptr<std::ostream> VFSFile::GetOutputStream(std::ios_base::openmode p_mode)
	{
		return m_loader->GetOutputStream(*this, p_mode);
	}

	void VFSFile::CloseStream(std::shared_ptr<std::istream>& p_stream)
	{
		m_loader->CloseStream(*this, p_stream);
	}

	void VFSFile::CloseStream(std::shared_ptr<std::ostream>& p_stream)
	{
		m_loader->CloseStream(*this, p_stream);
	}
}