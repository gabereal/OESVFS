/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "VFSRoot.h"

namespace oesvfs
{
	VFSRoot::VFSRoot() : m_loaders(), m_rootDirectories(), m_virtualEntries()
	{
	}

	VFSRoot::~VFSRoot()
	{
		Reset();
	}

	VFSRoot::VFSRoot(const VFSRoot & other) : m_loaders(other.m_loaders), m_rootDirectories(other.m_rootDirectories), m_virtualEntries(other.m_virtualEntries)
	{
	}

	VFSRoot::VFSRoot(VFSRoot && other) noexcept : m_loaders(), m_rootDirectories(), m_virtualEntries()
	{
		std::swap(m_loaders, other.m_loaders);
		std::swap(m_rootDirectories, other.m_rootDirectories);
		std::swap(m_virtualEntries, other.m_virtualEntries);
	}

	void VFSRoot::AddLoader(VFSPathLoader *& p_loader)
	{
		AddLoader(std::move(p_loader));
	}

	void VFSRoot::AddLoader(VFSPathLoader *&& p_loader)
	{
		if (p_loader != nullptr)
		{
			auto loader = std::shared_ptr<VFSPathLoader>(p_loader);
			m_loaders.push_back(loader);

			p_loader = nullptr;
		}
	}

	void VFSRoot::Reset()
	{
		m_rootDirectories.clear();
		m_loaders.clear();
	}

	void VFSRoot::Mount(std::string p_path, std::string p_mountPoint)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relPath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			VFSHelpers::FixDirectoryPath(p_mountPoint);
			auto entry = rootDir->GetEntry(relPath);

			if (entry != nullptr)
			{
				if (p_mountPoint == "")
				{
					m_virtualEntries[""] = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(entry, std::string("")));
				}
				else
				{
					std::string virtualBase = VFSHelpers::GetBaseDirectory(p_mountPoint);
					std::string virtualRest = VFSHelpers::GetRelativeDirectory(p_mountPoint);
					auto baseEntry = GetRootDirectory(virtualBase);
					std::shared_ptr<VFSVirtualEntry> virtualBaseEntry;

					if (baseEntry == nullptr)
					{
						virtualBaseEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(nullptr, virtualBase));
						m_virtualEntries[virtualBase] = virtualBaseEntry;
					}
					else
					{
						virtualBaseEntry = baseEntry->GetVirtualEntry();
					}

					virtualBase = VFSHelpers::GetBaseDirectory(virtualRest);
					virtualRest = VFSHelpers::GetRelativeDirectory(virtualRest);

					while (virtualRest != "")
					{
						auto virtualBaseEntryClone = virtualBaseEntry;

						if (virtualBaseEntryClone->IsDirectory())
						{
							auto virtualBaseSubEntry = std::static_pointer_cast<VFSDirectory>(virtualBaseEntryClone->GetEntry())->GetEntry(virtualBase);
							if (virtualBaseSubEntry != nullptr)
							{
								virtualBaseEntryClone = virtualBaseSubEntry->GetVirtualEntry();
							}
							else
							{
								virtualBaseEntryClone = nullptr;
							}
						}
						else
						{
							virtualBaseEntryClone = virtualBaseEntry->GetVirtualEntry(virtualBase);
						}

						if (virtualBaseEntryClone == nullptr)
						{
							virtualBaseEntry = virtualBaseEntry->AddVirtualEntry(std::shared_ptr<VFSEntry>(nullptr), virtualBase);
						}
						else
						{
							virtualBaseEntry = virtualBaseEntryClone;
						}

						virtualBase = VFSHelpers::GetBaseDirectory(virtualRest);

						if (virtualBase == "")
						{
							virtualBase = VFSHelpers::GetBaseDirectory(virtualRest + "/");
						}

						virtualRest = VFSHelpers::GetRelativeDirectory(virtualRest);
					}

					virtualBaseEntry->AddVirtualEntry(entry, virtualBase);
				}
			}
		}
	}

	bool VFSRoot::Unmount(std::string p_mountPoint)
	{
		VFSHelpers::FixDirectoryPath(p_mountPoint);
		std::string virtualBase = VFSHelpers::GetBaseDirectory(p_mountPoint);
		std::string virtualRest = VFSHelpers::GetRelativeDirectory(p_mountPoint);

		std::shared_ptr<VFSEntry> subEntry = nullptr;

		auto virtualMapEntry = m_virtualEntries.find(virtualBase);
		if (virtualMapEntry != m_virtualEntries.end())
		{
			if (virtualRest == "")
			{
				virtualMapEntry->second = nullptr;
				m_virtualEntries.erase(virtualMapEntry);

				return true;
			}
			else
			{
				subEntry = virtualMapEntry->second->GetEntry();
			}
		}
		else
		{
			subEntry = GetRootDirectory(virtualBase);
		}

		while (virtualRest != "")
		{
			virtualBase = VFSHelpers::GetBaseDirectory(virtualRest);
			virtualRest = VFSHelpers::GetRelativeDirectory(virtualRest);

			if (virtualRest == "")
			{
				subEntry->RemoveVirtualEntry(virtualBase);
				return true;
			}
			else
			{
				if (subEntry->IsDirectory())
				{
					subEntry = std::static_pointer_cast<VFSDirectory>(subEntry)->GetEntry(virtualBase);

					if (subEntry == nullptr)
					{
						return false;
					}
				}
				else
				{
					auto virtualSubEntry = subEntry->GetVirtualEntry(virtualBase);

					if (virtualSubEntry == nullptr)
					{
						return false;
					}

					while (virtualRest != "")
					{
						virtualBase = VFSHelpers::GetBaseDirectory(virtualRest);
						virtualRest = VFSHelpers::GetRelativeDirectory(virtualRest);

						if (virtualRest == "")
						{
							virtualSubEntry->RemoveVirtualEntry(virtualBase);
							return true;
						}
						else
						{
							virtualSubEntry = virtualSubEntry->GetVirtualEntry(virtualBase);

							if (virtualSubEntry == nullptr)
							{
								return false;
							}
						}
					}
				}
			}
		}

		return false;
	}

	std::shared_ptr<VFSDirectory> VFSRoot::GetDirectory(std::string p_path)
	{
		VFSHelpers::FixDirectoryPath(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->GetDirectory(relativePath);
		}
		else
		{
			return nullptr;
		}
	}

	std::shared_ptr<VFSFile> VFSRoot::GetFile(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->GetFile(relativePath);
		}
		else
		{
			return nullptr;
		}
	}

	void VFSRoot::Copy(std::string p_srcPath, std::string p_destPath)
	{
		VFSHelpers::ReplaceChar(p_srcPath);
		auto rootDir = GetRootDirectory(p_srcPath);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_srcPath);

		if (rootDir != nullptr)
		{
			rootDir->Copy(relativePath, p_destPath);
		}
	}

	std::shared_ptr<VFSDirectory> VFSRoot::OpenFile(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->OpenFile(relativePath);
		}
		else
		{
			return nullptr;
		}
	}

	std::shared_ptr<VFSDirectory> VFSRoot::CreateDirectory(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->CreateDirectory(relativePath);
		}
		else
		{
			return nullptr;
		}
	}

	std::shared_ptr<VFSFile> VFSRoot::CreateFile(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->CreateFile(relativePath);
		}
		else
		{
			return nullptr;
		}
	}

	bool VFSRoot::Exists(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->Exists(relativePath);
		}
		else
		{
			return false;
		}
	}

	std::uint64_t VFSRoot::FileSize(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->FileSize(relativePath);
		}
		else
		{
			return 0;
		}
	}

	bool VFSRoot::Remove(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->Remove(relativePath);
		}
		else
		{
			return false;
		}
	}

	bool VFSRoot::Rename(std::string p_oldPath, std::string p_newPath)
	{
		VFSHelpers::ReplaceChar(p_oldPath);
		auto rootDir = GetRootDirectory(p_oldPath);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_oldPath);

		if (rootDir != nullptr)
		{
			return rootDir->Rename(relativePath, p_newPath);
		}
		else
		{
			return false;
		}
	}

	bool VFSRoot::IsDirectory(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->IsDirectory(relativePath);
		}
		else
		{
			return false;
		}
	}

	bool VFSRoot::IsFile(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->IsFile(relativePath);
		}
		else
		{
			return false;
		}
	}

	bool VFSRoot::CanRead(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->CanRead(relativePath);
		}
		else
		{
			return false;
		}
	}

	bool VFSRoot::CanWrite(std::string p_path)
	{
		VFSHelpers::ReplaceChar(p_path);
		auto rootDir = GetRootDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);

		if (rootDir != nullptr)
		{
			return rootDir->CanWrite(relativePath);
		}
		else
		{
			return false;
		}
	}

	const std::vector<std::shared_ptr<VFSPathLoader>>& VFSRoot::GetLoaders()
	{
		return m_loaders;
	}

	VFSRoot & VFSRoot::operator=(const VFSRoot & other)
	{
		if (this != &other)
		{
			m_loaders = other.m_loaders;
			m_rootDirectories = other.m_rootDirectories;
			m_virtualEntries = other.m_virtualEntries;
		}

		return *this;
	}

	VFSRoot & VFSRoot::operator=(VFSRoot && other) noexcept
	{
		if (this != &other)
		{
			m_loaders = std::vector<std::shared_ptr<VFSPathLoader>>(std::move(other.m_loaders));
			m_rootDirectories = std::map<std::string, std::shared_ptr<VFSDirectory>>(std::move(other.m_rootDirectories));
			m_virtualEntries = std::map<std::string, std::shared_ptr<VFSVirtualEntry>>(std::move(other.m_virtualEntries));
		}

		return *this;
	}

	std::shared_ptr<VFSDirectory> VFSRoot::GetRootDirectory(std::string path)
	{
		auto name = VFSHelpers::GetBaseDirectory(path);

		auto virtualDir = m_virtualEntries.find(name);
		if (virtualDir == m_virtualEntries.end())
		{
			auto dir = m_rootDirectories.find(name);

			if (dir == m_rootDirectories.end())
			{
				for (auto& loader : m_loaders)
				{
					if (loader->CanReadPath(name))
					{
						auto nameWithoutSlash = name;

						if (VFSHelpers::ContainsChar(nameWithoutSlash, '/'))
						{
							nameWithoutSlash.erase(nameWithoutSlash.end() - 1);
						}

						auto newDir = std::shared_ptr<VFSDirectory>(new VFSDirectory(nameWithoutSlash, loader));
						m_rootDirectories[name] = newDir;
						newDir->Load();

						return newDir;
					}
				}

				return nullptr;
			}

			return dir->second;
		}

		return std::static_pointer_cast<VFSDirectory>(virtualDir->second->GetEntry());
	}
}