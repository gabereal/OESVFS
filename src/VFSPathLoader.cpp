/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSPathLoader.h"
#include "include/VFSRoot.h"

namespace oesvfs
{
	VFSPathLoader::VFSPathLoader(VFSRoot & p_root) : m_root(p_root)
	{
	}

	std::shared_ptr<VFSPathLoader> VFSPathLoader::GetThis()
	{
		return shared_from_this();
	}

	bool VFSPathLoader::CanLoad(VFSEntry & p_entry)
	{
		if (p_entry.IsDirectory())
		{
			return CanLoad(static_cast<VFSDirectory&>(p_entry));
		}
		else if (p_entry.IsFile())
		{
			return CanLoad(static_cast<VFSFile&>(p_entry));
		}

		return false;
	}

	std::shared_ptr<VFSDirectory> VFSPathLoader::OpenFile(VFSFile & p_file)
	{
		if (CanLoad(p_file))
		{
			return OpenFile_(p_file);
		}

		return nullptr;
	}

	std::shared_ptr<VFSDirectory> VFSPathLoader::CreateDirectory(VFSDirectory & p_rootEntry, std::string p_path)
	{
		auto res = CreateDirectory_(p_rootEntry, p_path);
		p_rootEntry.Load();
		return res;
	}

	std::shared_ptr<VFSFile> VFSPathLoader::CreateFile(VFSDirectory & p_rootEntry, std::string p_path)
	{
		auto res = CreateFile_(p_rootEntry, p_path);
		p_rootEntry.Load();
		return res;
	}

	bool VFSPathLoader::Load(VFSFile & p_file)
	{
		if (CanLoad(p_file))
		{
			return Load_(p_file);
		}

		return false;
	}

	bool VFSPathLoader::Load(VFSDirectory & p_directory)
	{
		if (CanLoad(p_directory))
		{
			return Load_(p_directory);
		}

		return false;
	}


	void VFSPathLoader::Copy(VFSEntry & p_entry, std::shared_ptr<VFSEntry> p_dest)
	{
		if (p_dest != nullptr && p_entry.CanRead() && p_dest->CanWrite())
		{
			if (p_entry.IsFile())
			{
				Copy(static_cast<VFSFile&>(p_entry), std::static_pointer_cast<VFSFile>(p_dest));
			}
			else
			{
				Copy(static_cast<VFSDirectory&>(p_entry), std::static_pointer_cast<VFSFile>(p_dest));
			}
		}
	}

	void VFSPathLoader::Copy(VFSFile & p_file, std::shared_ptr<VFSEntry> p_dest)
	{
		if (p_dest != nullptr && p_file.CanRead() && p_dest->CanWrite())
		{
			if (p_dest->IsFile())
			{
				CopyFile_(p_file, std::static_pointer_cast<VFSFile>(p_dest));
			}
			else if (p_dest->IsDirectory())
			{
				CopyFile_(p_file, std::static_pointer_cast<VFSDirectory>(p_dest));
			}

			p_dest->Load();
		}
	}

	void VFSPathLoader::Copy(VFSDirectory & p_dir, std::shared_ptr<VFSEntry> p_dest)
	{
		if (p_dest != nullptr && p_dir.CanRead() && p_dest->CanWrite())
		{
			if (p_dest->IsDirectory())
			{
				CopyDirectory_(p_dir, std::static_pointer_cast<VFSDirectory>(p_dest));
				p_dest->Load();
			}
		}
	}

	std::uint64_t VFSPathLoader::Size(VFSEntry & p_entry)
	{
		if (p_entry.IsFile())
		{
			return Size_(static_cast<VFSFile&>(p_entry));
		}
		else
		{
			return Size_(static_cast<VFSDirectory&>(p_entry));
		}
	}

	bool VFSPathLoader::Remove(VFSDirectory& p_parent, VFSEntry & p_entry)
	{
		if (p_entry.IsFile())
		{
			return Remove_(p_parent, static_cast<VFSFile&>(p_entry));
			p_parent.Load();
		}
		else if (p_entry.IsDirectory())
		{
			return Remove_(p_parent, static_cast<VFSDirectory&>(p_entry));
			p_parent.Load();
		}

		return false;
	}

	std::shared_ptr<VFSPathLoader> VFSPathLoader::FindSuitableLoader(VFSEntry& p_entry)
	{
		if (CanLoad(p_entry))
		{
			return GetThis();
		}
		else
		{
			auto& loaders = m_root.GetLoaders();

			for (auto& loader : loaders)
			{
				if (loader->CanLoad(p_entry))
				{
					return std::const_pointer_cast<VFSPathLoader>(loader);
				}
			}

			return nullptr;
		}
	}

	std::uint64_t VFSPathLoader::Size_(VFSDirectory & p_entry)
	{
		std::uint64_t size = 0;

		for (auto& entry : p_entry)
		{
			if (entry.second != nullptr)
			{
				size += entry.second->FileSize();
			}
		}

		return size;
	}
}