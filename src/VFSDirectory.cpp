/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSDirectory.h"
#include "include/VFSVirtualEntry.h"
#include "include/VFSPathLoader.h"
#include "include/VFSHelpers.h"
#include "include/VFSFile.h"

namespace oesvfs
{
    VFSDirectory::VFSDirectory(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader) : VFSEntry(p_parentDir, p_name, p_loader)
    {
    }

	VFSDirectory::~VFSDirectory()
	{
		m_cachedEntries.clear();
	}

    std::shared_ptr<VFSDirectory> VFSDirectory::GetDirectory(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if(entry != nullptr && entry->IsDirectory())
        {
            return std::static_pointer_cast<VFSDirectory>(entry);
        }
        else
        {
			return nullptr;
        }
    }

    std::shared_ptr<VFSFile> VFSDirectory::GetFile(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if (entry != nullptr && entry->IsFile())
        {
            return std::static_pointer_cast<VFSFile>(entry);
        }
        else
        {
			return nullptr;
        }
    }

	bool VFSDirectory::CanRead(std::string p_path)
	{
		auto entry = GetEntry(p_path);

		if (entry != nullptr)
		{
			return entry->CanRead();
		}

		return false;
	}

	bool VFSDirectory::CanWrite(std::string p_path)
	{
		auto entry = GetEntry(p_path);

		if (entry != nullptr)
		{
			return entry->CanWrite();
		}

		return false;
	}

    void VFSDirectory::Copy(std::string p_srcPath, std::string p_destPath)
    {
        auto srcEntry = GetEntry(p_srcPath);
        auto destEntry = GetEntry(p_destPath);

		if (srcEntry != nullptr)
		{
			srcEntry->CopyTo(destEntry);
		}
    }

    bool VFSDirectory::Exists(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if(entry != nullptr)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    std::uint64_t VFSDirectory::FileSize(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if (entry != nullptr)
        {
            return entry->FileSize();
        }
        else
        {
            return 0;
        }
    }

    bool VFSDirectory::Remove(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if(entry != nullptr)
        {
            if(IsOwnEntry(entry))
            {
                entry->Remove();

                return true;
            }
            else
            {
				auto subDir = VFSHelpers::GetBaseDirectory(p_path);
				auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
                auto& subDirEntry = m_cachedEntries[subDir];

                return std::static_pointer_cast<VFSDirectory>(subDirEntry)->Remove(relativePath);
            }
        }

        return false;
    }

    bool VFSDirectory::Rename(std::string p_path, std::string p_newName)
    {
        auto entry = GetEntry(p_path);

        if(entry != nullptr)
        {
            if (IsOwnEntry(entry))
            {
                if (entry->Rename(p_newName))
                {
                    auto &mapEntry = m_cachedEntries.extract(p_path);
                    mapEntry.key() = p_newName;
                    m_cachedEntries.insert(std::move(mapEntry));

                    return true;
                }
            }
            else
            {
                auto subDir = VFSHelpers::GetBaseDirectory(p_path);
				auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
                auto &subDirEntry = m_cachedEntries[subDir];

                auto ret = std::static_pointer_cast<VFSDirectory>(subDirEntry)->Rename(relativePath, p_newName);

				if (ret)
				{
					m_name = p_newName;
				}

				return ret;
            }
        }

        return false;
    }

    bool VFSDirectory::IsDirectory(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if (entry != nullptr)
        {
            return entry->IsDirectory();
        }

        return false;
    }

    bool VFSDirectory::IsFile(std::string p_path)
    {
        auto entry = GetEntry(p_path);

        if (entry != nullptr)
        {
            return entry->IsFile();
        }

        return false;
    }

	std::shared_ptr<VFSDirectory> VFSDirectory::OpenFile(std::string p_path)
	{
		if (VFSHelpers::ContainsChar(p_path, '/'))
		{
			auto subDirName = VFSHelpers::GetBaseDirectory(p_path);
			auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
			std::shared_ptr<VFSDirectory> subDir = GetDirectory(subDirName);

			if (subDir != nullptr)
			{
				return subDir->OpenFile(relativePath);
			}
			else
			{
				return nullptr;
			}
		}
		else
		{
			std::shared_ptr<VFSFile> file = GetFile(p_path);
			return file->Open();
		}
	}

    std::shared_ptr<VFSDirectory> VFSDirectory::CreateDirectory(std::string p_path)
    {
        if (VFSHelpers::ContainsChar(p_path, '/'))
        {
            auto subDirName = VFSHelpers::GetBaseDirectory(p_path);
			auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
            std::shared_ptr<VFSDirectory> subDir = GetDirectory(subDirName);

            if (subDir != nullptr)
            {
                return subDir->CreateDirectory(relativePath);
            }
            else
            {
                return nullptr;
            }
        }
        else
        {
			if (!IsLoaded())
			{
				Load();
			}

			return m_loader->CreateDirectory(*this, p_path);
        }
    }

    std::shared_ptr<VFSFile> VFSDirectory::CreateFile(std::string p_path)
    {
        if(VFSHelpers::ContainsChar(p_path, '/'))
        {
            auto subDirName = VFSHelpers::GetBaseDirectory(p_path);
			auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
			std::shared_ptr<VFSDirectory> subDir = GetDirectory(subDirName);

            if(subDir != nullptr)
            {
                return subDir->CreateFile(relativePath);
            }
            else
            {
                return nullptr;
            }
        }
        else
        {
			if (!IsLoaded())
			{
				Load();
			}

			return m_loader->CreateFile(*this, p_path);
        }
    }

	std::shared_ptr<VFSFile> VFSDirectory::CreateFile(std::string p_path, std::istream & p_input)
	{
		if (VFSHelpers::ContainsChar(p_path, '/'))
		{
			auto subDirName = VFSHelpers::GetBaseDirectory(p_path);
			auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
			std::shared_ptr<VFSDirectory> subDir = GetDirectory(subDirName);

			if (subDir != nullptr)
			{
				return subDir->CreateFile(relativePath, p_input);
			}
			else
			{
				return nullptr;
			}
		}
		else
		{
			if (!IsLoaded())
			{
				Load();
			}

			return std::static_pointer_cast<VFSFile>(m_loader->CreateFile(*this, p_path, p_input));
		}
	}

    VFSDirectory::iterator VFSDirectory::begin()
    {
        return m_cachedEntries.begin();
    }

    VFSDirectory::iterator VFSDirectory::end()
    {
        return m_cachedEntries.end();
    }

    VFSDirectory::const_iterator VFSDirectory::begin() const
    {
        return m_cachedEntries.begin();
    }

    VFSDirectory::const_iterator VFSDirectory::end() const
    {
        return m_cachedEntries.end();
    }

    VFSDirectory::reverse_iterator VFSDirectory::rbegin()
    {
        return m_cachedEntries.rbegin();
    }

    VFSDirectory::reverse_iterator VFSDirectory::rend()
    {
        return m_cachedEntries.rend();
    }

    VFSDirectory::const_reverse_iterator VFSDirectory::rbegin() const
    {
        return m_cachedEntries.rbegin();
    }

    VFSDirectory::const_reverse_iterator VFSDirectory::rend() const
    {
        return m_cachedEntries.rend();
    }

	VFSDirectory::VFSDirectory(std::string p_name, std::shared_ptr<VFSPathLoader> p_loader) : VFSEntry(p_name, p_loader)
	{
	}

    std::shared_ptr<VFSEntry> VFSDirectory::GetEntry(std::string p_path)
    {
		if (!IsLoaded())
		{
			Load();
		}

		if (p_path == "")
		{
			return GetThis();
		}

		auto subDirName = VFSHelpers::GetBaseDirectory(p_path);
		auto relativePath = VFSHelpers::GetRelativeDirectory(p_path);
		auto virtualEntry = GetVirtualEntry(subDirName);

		if (virtualEntry == nullptr || virtualEntry->GetEntry() == nullptr)
		{
			if (subDirName != "")
			{
				auto &mapEntry = m_cachedEntries.find(subDirName);

				if (mapEntry == m_cachedEntries.end() || mapEntry->second == nullptr || !mapEntry->second->IsDirectory())
				{
					return nullptr;
				}
				else
				{
					return std::static_pointer_cast<VFSDirectory>(mapEntry->second)->GetEntry(relativePath);
				}
			}
			else
			{
				auto &mapEntry = m_cachedEntries.find(p_path);

				if (mapEntry == m_cachedEntries.end())
				{
					mapEntry = m_cachedEntries.find(p_path + "/");

					if (mapEntry == m_cachedEntries.end())
					{
						return nullptr;
					}
				}

				if (!mapEntry->second->IsLoaded())
				{
					mapEntry->second->Load();
				}

				return mapEntry->second;
			}
		}
		else
		{
			return virtualEntry->GetEntry();
		}
    }

	bool VFSDirectory::IsOwnEntry(const std::shared_ptr<VFSEntry>& p_entry)
	{
		auto entryName = p_entry->GetName();

		if (m_cachedEntries.find(entryName) == m_cachedEntries.end())
		{
			return false;
		}

		return true;
	}

	VFSDirectory::entry_map & VFSDirectory::GetMap()
	{
		return m_cachedEntries;
	}
}