/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSHelpers.h"
#include<algorithm>
#include<filesystem>

namespace fs = std::filesystem;

namespace oesvfs
{
	void VFSHelpers::FixDirectoryPath(std::string & p_str)
	{
		ReplaceChar(p_str);

		if (p_str.end() != p_str.begin() && *(p_str.end() - 1) != '/')
		{
			p_str += "/";
		}
	}

	void VFSHelpers::ReplaceChar(std::string & p_str, const char p_orig, const char p_new)
	{
		std::replace(p_str.begin(), p_str.end(), p_orig, p_new);
	}

	bool VFSHelpers::ContainsChar(const std::string & p_str, const char p_chr)
	{
		return ContainsChar(std::move(p_str), p_chr);
	}

	bool VFSHelpers::ContainsChar(const std::string && p_str, const char p_chr)
	{
		for (const char& ch : p_str)
		{
			if (ch == p_chr)
			{
				return true;
			}
		}

		return false;
	}

	std::string VFSHelpers::GetRootName(const std::string & p_str)
	{
		return GetRootName(std::move(p_str));
	}

	std::string VFSHelpers::GetCurrentDirectory()
	{
		return fs::current_path().string();
	}

	std::string VFSHelpers::GetRootName(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.root_name().string();
	}

	std::string VFSHelpers::GetRootDir(const std::string & p_str)
	{
		return GetRootDir(std::move(p_str));
	}

	std::string VFSHelpers::GetRootDir(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.root_directory().string();
	}

	std::string VFSHelpers::GetRootPath(const std::string & p_str)
	{
		return GetRootPath(std::move(p_str));
	}

	std::string VFSHelpers::GetRootPath(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.root_path().string();
	}

	std::string VFSHelpers::GetRelativePath(const std::string & p_str)
	{
		return GetRelativePath(std::move(p_str));
	}

	std::string VFSHelpers::GetRelativePath(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.relative_path().string();
	}

	std::string VFSHelpers::GetParentPath(const std::string & p_str)
	{
		return GetParentPath(std::move(p_str));
	}

	std::string VFSHelpers::GetParentPath(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.parent_path().string();
	}

	std::string VFSHelpers::GetFilename(const std::string & p_str)
	{
		return GetFilename(std::move(p_str));
	}

	std::string VFSHelpers::GetFilename(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.filename().string();
	}

	std::string VFSHelpers::GetStem(const std::string & p_str)
	{
		return GetStem(std::move(p_str));
	}

	std::string VFSHelpers::GetStem(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.stem().string();
	}

	std::string VFSHelpers::GetExtension(const std::string & p_str)
	{
		return GetExtension(std::move(p_str));
	}

	std::string VFSHelpers::GetExtension(const std::string && p_str)
	{
		fs::path p(p_str);
		return p.extension().string();
	}

	std::string VFSHelpers::GetBaseDirectory(const std::string & p_str)
	{
		return GetBaseDirectory(std::move(p_str));
	}

	std::string VFSHelpers::GetBaseDirectory(const std::string && p_str)
	{
		for (auto it = p_str.begin(); it != p_str.end(); ++it)
		{
			if ((*it == '/' || *it == '\\') && it != p_str.begin())
			{
				return std::string(p_str.begin(), it + 1);
			}
		}

		return std::string();
	}

	std::string VFSHelpers::GetRelativeDirectory(const std::string & p_str)
	{
		return GetRelativeDirectory(std::move(p_str));
	}

	std::string VFSHelpers::GetRelativeDirectory(const std::string && p_str)
	{
		for (auto it = p_str.begin(); it != p_str.end(); ++it)
		{
			if ((*it == '/' || *it == '\\') && it != p_str.begin())
			{
				return std::string(it + 1, p_str.end());
			}
		}

		return std::string();
	}

}