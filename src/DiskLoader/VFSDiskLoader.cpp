/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "DiskLoader/VFSDiskLoader.h"
#include "VFSEntry.h"
#include "VFSFile.h"
#include "VFSDirectory.h"
#include "VFSHelpers.h"

#include<filesystem>
#include<iostream>
#include<fstream>

namespace fs = std::filesystem;

namespace oesvfs
{
	VFSDiskLoader::VFSDiskLoader(VFSRoot & p_root) : VFSPathLoader(p_root), m_openStreams()
	{
	}

	bool VFSDiskLoader::CanLoad(VFSDirectory & p_dir)
	{
		return fs::exists(p_dir.GetFullPath());
	}

	bool VFSDiskLoader::CanLoad(VFSFile & p_file)
	{
		return false;
	}

	bool VFSDiskLoader::CanReadPath(std::string p_path)
	{
		return fs::exists(p_path);
	}

	std::shared_ptr<VFSDirectory> VFSDiskLoader::OpenFile(VFSFile& p_file)
	{
		return nullptr;
	}

	std::shared_ptr<VFSFile> VFSDiskLoader::CreateFile(VFSDirectory& p_rootEntry, std::string p_relativePath, std::istream& p_input)
	{
		auto newEntry = VFSPathLoader::CreateFile(p_rootEntry, p_relativePath);

		if(newEntry == nullptr)
		{
			return nullptr;
		}

		auto newFile = std::static_pointer_cast<VFSFile>(newEntry);
		std::shared_ptr<std::ostream> fileStream = newFile->GetOutputStream();

		char c;
		while(p_input.get(c))
		{
			fileStream->put(c);
		}

		newFile->CloseStream(fileStream);

		return newEntry;
	}

	bool VFSDiskLoader::Rename(VFSDirectory& p_parent, VFSEntry & p_entry, std::string p_newName)
	{
		fs::path entryPath(p_entry.GetFullPath());

		if (fs::exists(entryPath))
		{
			if (HasOpenStreams(p_entry))
			{
				return false;
			}

			entryPath.replace_filename(VFSHelpers::GetFilename(p_newName));

			fs::rename(p_entry.GetFullPath(), entryPath);

			p_parent.Load();

			return true;
		}

		return false;
	}

	bool VFSDiskLoader::IsDirectory(VFSEntry & p_entry)
	{
		return fs::is_directory(p_entry.GetFullPath());
	}

	bool VFSDiskLoader::IsFile(VFSEntry & p_entry)
	{
		return fs::is_regular_file(p_entry.GetFullPath());
	}

	bool VFSDiskLoader::CanRead(VFSEntry & p_entry)
	{
		auto stat = fs::status(p_entry.GetFullPath()).permissions();
		fs::perms readPerms = fs::perms::owner_read | fs::perms::group_read | fs::perms::others_read;

		return ((stat & readPerms) != fs::perms::none);
	}

	bool VFSDiskLoader::CanWrite(VFSEntry & p_entry)
	{
		auto stat = fs::status(p_entry.GetFullPath()).permissions();
		fs::perms writePerms = fs::perms::owner_write | fs::perms::group_write | fs::perms::others_write;

		return ((stat & writePerms) != fs::perms::none);
	}

	std::shared_ptr<std::istream> VFSDiskLoader::GetInputStream(VFSFile & p_entry, std::ios_base::openmode p_mode)
	{
		if (fs::exists(p_entry.GetFullPath()))
		{
			auto str = std::shared_ptr<std::ifstream>(new std::ifstream(p_entry.GetFullPath(), p_mode));

			if (str != nullptr)
			{
				auto mapEntry = m_openStreams.find(p_entry.GetFullPath());

				if (mapEntry == m_openStreams.end())
				{
					m_openStreams[p_entry.GetFullPath()] = 1;
				}
				else
				{
					mapEntry->second++;
				}
			}

			return str;
		}

		return std::shared_ptr<std::istream>(nullptr);
	}

	std::shared_ptr<std::ostream> VFSDiskLoader::GetOutputStream(VFSFile & p_entry, std::ios_base::openmode p_mode)
	{
		if (fs::exists(p_entry.GetFullPath()))
		{
			auto str = std::shared_ptr<std::ostream>(new std::ofstream(p_entry.GetFullPath(), p_mode));

			if (str != nullptr)
			{
				auto mapEntry = m_openStreams.find(p_entry.GetFullPath());

				if (mapEntry == m_openStreams.end())
				{
					m_openStreams[p_entry.GetFullPath()] = 1;
				}
				else
				{
					mapEntry->second++;
				}
			}

			return str;
		}

		return std::shared_ptr<std::ostream>(nullptr);
	}

	void VFSDiskLoader::CloseStream(VFSFile & p_file, std::shared_ptr<std::istream>& p_stream)
	{
		auto mapEntry = m_openStreams.find(p_file.GetFullPath());

		if (mapEntry == m_openStreams.end())
		{
			return;
		}
		else
		{
			p_stream = nullptr;
			mapEntry->second--;

			if (mapEntry->second == 0)
			{
				m_openStreams.erase(mapEntry);
			}
		}
	}

	void VFSDiskLoader::CloseStream(VFSFile & p_file, std::shared_ptr<std::ostream>& p_stream)
	{
		auto mapEntry = m_openStreams.find(p_file.GetFullPath());

		if (mapEntry == m_openStreams.end())
		{
			return;
		}
		else
		{
			p_stream = nullptr;
			mapEntry->second--;

			if (mapEntry->second == 0)
			{
				m_openStreams.erase(mapEntry);
			}
		}
	}

	std::shared_ptr<VFSFile> VFSDiskLoader::CreateFile_(VFSDirectory & p_rootEntry, std::string p_relativePath)
	{
		std::string filePath = p_rootEntry.GetFullPath() + "/" + p_relativePath;

		if (fs::exists(filePath))
		{
			if (fs::is_directory(filePath))
			{
				fs::remove_all(filePath);
			}
			else
			{
				fs::remove(filePath);
			}
		}

		std::ofstream file(filePath);
		return std::shared_ptr<VFSFile>(new VFSFile(&p_rootEntry, p_relativePath, GetThis()));
	}

	bool VFSDiskLoader::Load_(VFSFile & p_file)
	{
		return false;
	}

	bool VFSDiskLoader::Load_(VFSDirectory & p_dir)
	{
		auto directoryPath = p_dir.GetFullPath();
		auto& map = p_dir.GetMap();

		if (VFSHelpers::ContainsChar(directoryPath, ':') && !VFSHelpers::ContainsChar(directoryPath, '/'))
		{
			directoryPath += "/";
		}
		
		for (auto& it = map.begin(); it != map.end();)
		{
			if (!fs::exists(it->second->GetFullPath()))
			{
				map.erase(it++);
			}
			else
			{
				++it;
			}
		}

		for (auto& path : fs::directory_iterator(directoryPath))
		{
			auto name = VFSHelpers::GetFilename(path.path().string());

			try
			{
				if (fs::is_directory(path))
				{
					if (map.find(name + "/") == map.end())
					{
						map[name + "/"] = std::shared_ptr<VFSDirectory>(new VFSDirectory(&p_dir, name, GetThis()));
					}
				}
				else if (fs::is_regular_file(path))
				{
					if (map.find(name) == map.end())
					{
						map[name] = std::shared_ptr<VFSFile>(new VFSFile(&p_dir, name, GetThis()));
					}
				}
			}
			catch(std::exception)
			{ }
		}

		return true;
	}

	void VFSDiskLoader::CopyFile_(VFSFile & p_file, std::shared_ptr<VFSFile> p_dest)
	{
		auto inputStream = p_file.GetInputStream();
		auto outputStream = p_dest->GetOutputStream();

		if (inputStream == nullptr || outputStream == nullptr)
		{
			return;
		}

		char c;
		while (inputStream->get(c))
		{
			outputStream->put(c);
		}
	}

	void VFSDiskLoader::CopyFile_(VFSFile & p_file, std::shared_ptr<VFSDirectory> p_dest)
	{
		auto file = p_dest->GetFile(p_file.GetName());

		if (file == nullptr)
		{
			auto inputStream = p_file.GetInputStream();

			if (inputStream == nullptr)
			{
				return;
			}

			p_dest->CreateFile(p_file.GetName(), *inputStream);

			p_file.CloseStream(inputStream);
		}
		else
		{
			CopyFile_(p_file, file);
		}
	}

	void VFSDiskLoader::CopyDirectory_(VFSDirectory & p_dir, std::shared_ptr<VFSDirectory> p_dest)
	{
		auto subDir = p_dest->CreateDirectory(p_dir.GetName());

		for (auto& entry : p_dir)
		{
			entry.second->CopyTo(subDir);
		}
	}

	std::uint64_t VFSDiskLoader::Size_(VFSFile & p_file)
	{
		return fs::file_size(p_file.GetFullPath());
	}

	bool VFSDiskLoader::Remove_(VFSDirectory & p_parent, VFSDirectory & p_dir)
	{
		if (HasOpenStreams(p_dir))
		{
			return false;
		}

		return (fs::remove_all(p_dir.GetFullPath()) != 0);
	}

	bool VFSDiskLoader::Remove_(VFSDirectory & p_parent, VFSFile & p_file)
	{
		if (HasOpenStreams(p_file))
		{
			return false;
		}

		return fs::remove(p_file.GetFullPath());
	}

	bool VFSDiskLoader::HasOpenStreams(VFSEntry & p_entry)
	{
		if (p_entry.IsDirectory())
		{
			return HasOpenStreams(static_cast<VFSDirectory&>(p_entry));
		}
		else if (p_entry.IsFile())
		{
			return HasOpenStreams(static_cast<VFSFile&>(p_entry));
		}
		else
		{
			return false;
		}
	}

	bool VFSDiskLoader::HasOpenStreams(VFSFile & p_file)
	{
		auto mapEntry = m_openStreams.find(p_file.GetFullPath());

		if (mapEntry == m_openStreams.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool VFSDiskLoader::HasOpenStreams(VFSDirectory & p_dir)
	{
		for (auto& entry : p_dir)
		{
			if (HasOpenStreams(*entry.second))
			{
				return true;
			}
		}

		return false;
	}

	std::shared_ptr<VFSDirectory> VFSDiskLoader::OpenFile_(VFSFile & p_file)
	{
		return nullptr;
	}

	std::shared_ptr<VFSDirectory> VFSDiskLoader::CreateDirectory_(VFSDirectory & p_rootEntry, std::string p_relativePath)
	{
		std::string filePath = p_rootEntry.GetFullPath() + "/" + p_relativePath;

		if (fs::exists(filePath))
		{
			if (fs::is_directory(filePath))
			{
				fs::remove_all(filePath);
			}
			else
			{
				fs::remove(filePath);
			}
		}

		if (fs::create_directory(filePath))
		{
			return std::shared_ptr<VFSDirectory>(new VFSDirectory(&p_rootEntry, p_relativePath, GetThis()));
		}

		return nullptr;
	}

}