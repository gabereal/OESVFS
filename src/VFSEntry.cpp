/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSEntry.h"
#include "include/VFSFile.h"
#include "include/VFSDirectory.h"
#include "include/VFSVirtualEntry.h"
#include "include/VFSPathLoader.h"

namespace oesvfs
{
	VFSEntry::VFSEntry(VFSDirectory* p_parentDir, std::string &p_name, std::shared_ptr<VFSPathLoader> p_loader) : m_loaded(false), m_name(p_name), m_loader(p_loader), m_parentDir(p_parentDir), m_virtualEntry(nullptr)
	{
		if (p_parentDir == nullptr)
		{
			throw std::invalid_argument("Parent Directory can not be null pointer.");
		}
	}

	VFSEntry::~VFSEntry()
	{
		m_loaded = false;
		m_loader = nullptr;
		m_virtualEntry = nullptr;
		m_parentDir = nullptr;
	}

	std::string VFSEntry::GetName()
	{
		return m_name;
	}

	std::string VFSEntry::GetFullPath()
	{
		std::string path = "";

		if (m_parentDir != nullptr)
		{
			path = m_parentDir->GetFullPath();
			path += "/";
		}

		path += m_name;

		return path;
	}

	std::shared_ptr<VFSPathLoader> VFSEntry::GetLoader()
	{
		return m_loader;
	}

	VFSDirectory* VFSEntry::GetParent()
	{
		return m_parentDir;
	}

	void VFSEntry::RemoveVirtualEntry(std::string p_name)
	{
		if (m_virtualEntry == nullptr)
		{
			m_virtualEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(shared_from_this(), m_name));
		}

		m_virtualEntry->RemoveVirtualEntry(p_name);
	}

	std::shared_ptr<VFSVirtualEntry> VFSEntry::GetVirtualEntry()
	{
		if (m_virtualEntry == nullptr)
		{
			m_virtualEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(shared_from_this(), m_name));
		}

		return m_virtualEntry;
	}

	std::shared_ptr<VFSVirtualEntry> VFSEntry::GetVirtualEntry(std::string p_name)
	{
		if (m_virtualEntry == nullptr)
		{
			m_virtualEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(shared_from_this(), m_name));
		}

		return m_virtualEntry->GetVirtualEntry(p_name);
	}

	std::shared_ptr<VFSVirtualEntry> VFSEntry::AddVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName)
	{
		if (m_virtualEntry == nullptr)
		{
			m_virtualEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(shared_from_this(), m_name));
		}

		return m_virtualEntry->AddVirtualEntry(p_entry, p_virtualName);
	}

	std::shared_ptr<VFSEntry> VFSEntry::GetThis()
	{
		return shared_from_this();
	}

	void VFSEntry::Load()
	{
		if (IsDirectory())
		{
			m_loader->Load(static_cast<VFSDirectory&>(*this));
			m_loaded = true;
		}
		else if (IsFile())
		{
			m_loader->Load(static_cast<VFSFile&>(*this));
			m_loaded = true;
		}
	}

	void VFSEntry::CopyTo(const std::shared_ptr<VFSEntry> p_dest)
	{
		m_loader->Copy(*this, p_dest);
	}

	std::uint64_t VFSEntry::FileSize()
	{
		return m_loader->Size(*this);
	}

	bool VFSEntry::Remove()
	{
		if (m_parentDir != nullptr)
		{
			return m_loader->Remove(*m_parentDir, *this);
		}

		return false;
	}

	bool VFSEntry::Rename(std::string p_newName)
	{
		if (m_parentDir != nullptr)
		{
			if (m_loader->Rename(*m_parentDir, *this, p_newName))
			{
				m_name = p_newName;
				return true;
			}
		}

		return false;
	}

	bool VFSEntry::IsDirectory()
	{
		return m_loader->IsDirectory(*this);
	}

	bool VFSEntry::IsFile()
	{
		return m_loader->IsFile(*this);
	}

	bool VFSEntry::IsLoaded()
	{
		return m_loaded;
	}

	bool VFSEntry::CanRead()
	{
		return m_loader->CanRead(*this);
	}

	bool VFSEntry::CanWrite()
	{
		return m_loader->CanWrite(*this);
	}

	VFSEntry::VFSEntry(std::string p_name, std::shared_ptr<VFSPathLoader> p_loader) : m_loaded(false), m_name(p_name), m_loader(p_loader), m_parentDir(nullptr), m_virtualEntry(nullptr)
	{
	}

}