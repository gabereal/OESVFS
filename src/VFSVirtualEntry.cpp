/*
 * MIT License
 * 
 * Copyright (c) 2018 GabeReal
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "include/VFSVirtualEntry.h"

namespace oesvfs
{
	VFSVirtualEntry::VFSVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName) : m_entry(p_entry), m_fullVirtualPath(p_virtualName), m_virtualName(p_virtualName)
	{
	}

	VFSVirtualEntry::VFSVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_fullVirtualPath, std::string p_virtualName) : m_entry(p_entry),  m_fullVirtualPath(p_fullVirtualPath), m_virtualName(p_virtualName)
	{
	}

	VFSVirtualEntry::~VFSVirtualEntry()
	{
		m_entry = nullptr;
		m_virtualEntries.clear();
	}

	void VFSVirtualEntry::SetEntry(std::shared_ptr<VFSEntry> p_entry)
	{
		m_entry = p_entry;
	}

	std::shared_ptr<VFSEntry> VFSVirtualEntry::GetEntry()
	{
		return m_entry;
	}

	std::string VFSVirtualEntry::GetName()
	{
		if (m_entry != nullptr)
		{
			return m_entry->GetName();
		}
		else
		{
			return "";
		}
	}

	std::string VFSVirtualEntry::GetFullPath()
	{
		if (m_entry != nullptr)
		{
			return m_entry->GetFullPath();
		}
		else
		{
			return "";
		}
	}

	std::shared_ptr<VFSPathLoader> VFSVirtualEntry::GetLoader()
	{
		if (m_entry != nullptr)
		{
			return m_entry->GetLoader();
		}
		else
		{
			return nullptr;
		}
	}

	void VFSVirtualEntry::RemoveVirtualEntry(std::string p_name)
	{
		m_virtualEntries.erase(p_name);
	}

	std::shared_ptr<VFSVirtualEntry> VFSVirtualEntry::GetVirtualEntry(std::string p_name)
	{
		auto entry = m_virtualEntries.find(p_name);

		if (entry == m_virtualEntries.end())
		{
			return nullptr;
		}
		else
		{
			return entry->second;
		}
	}

	std::shared_ptr<VFSVirtualEntry> VFSVirtualEntry::AddVirtualEntry(std::shared_ptr<VFSEntry> p_entry, std::string p_virtualName)
	{
		auto virtualEntry = std::shared_ptr<VFSVirtualEntry>(new VFSVirtualEntry(p_entry, m_fullVirtualPath + "/" + p_virtualName, p_virtualName));
		m_virtualEntries[p_virtualName] = virtualEntry;
		return virtualEntry;
	}

	std::string VFSVirtualEntry::GetVirtualName()
	{
		return m_virtualName;
	}

	std::string VFSVirtualEntry::GetFullVirtualPath()
	{
		return m_fullVirtualPath;
	}

	void VFSVirtualEntry::Load()
	{
		if (m_entry != nullptr)
		{
			m_entry->Load();
		}
	}

	void VFSVirtualEntry::CopyTo(const std::shared_ptr<VFSVirtualEntry> p_dest)
	{
		if (m_entry != nullptr && p_dest != nullptr)
		{
			m_entry->CopyTo(p_dest->GetEntry());
		}
	}

	std::uint64_t VFSVirtualEntry::FileSize()
	{
		if (m_entry != nullptr)
		{
			return m_entry->FileSize();
		}
		else
		{
			return 0;
		}
	}

	bool VFSVirtualEntry::Remove()
	{
		if (m_entry != nullptr)
		{
			return m_entry->Remove();
		}
		else
		{
			return false;
		}
	}

	bool VFSVirtualEntry::Rename(std::string p_newName)
	{
		if (m_entry != nullptr)
		{
			return m_entry->Rename(p_newName);
		}
		else
		{
			return false;
		}
	}

	void VFSVirtualEntry::Reset()
	{
		m_virtualEntries.clear();
	}

	bool VFSVirtualEntry::Exists()
	{
		return (m_entry != nullptr);
	}

	bool VFSVirtualEntry::IsDirectory()
	{
		if (m_entry != nullptr)
		{
			return m_entry->IsDirectory();
		}
		else
		{
			return false;
		}
	}

	bool VFSVirtualEntry::IsFile()
	{
		if (m_entry != nullptr)
		{
			return m_entry->IsFile();
		}
		else
		{
			return false;
		}
	}

	bool VFSVirtualEntry::CanRead()
	{
		if (m_entry != nullptr)
		{
			return m_entry->CanRead();
		}
		else
		{
			return false;
		}
	}

	bool VFSVirtualEntry::CanWrite()
	{
		if (m_entry != nullptr)
		{
			return m_entry->CanWrite();
		}
		else
		{
			return false;
		}
	}

}